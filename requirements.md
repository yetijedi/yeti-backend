Requirements

1. Users management
* Authentication via phone number, only for customers
* Add SM authentication in 2nd phase
* Add studiing field, or working field - 3th phase
* Users have roles (Admin, Manager, Employer, Customer)
* Only Admin can register a new Manager
* Administrator/Manager can register a new Employer
* Employers must be assigned to a specific shop
* Assign shop after customer login
* Employers must to check in work.
* Customers have bonus system
* Generate QR code for each customer
* Employers can have salary fixed/percent
* Cutomers can have history.
* Users must have permissions for all pages.
* Employer pre-order *
* Add bonuses

2. Products
* Only Admin or Manager can add or temporary remove products
* Products (**Ingredients** and **Sell Product**)
* Place where can build products which used other kind of products    *
* Products can assigned to a specific shop.
* **Sell Product's** can have **Ingredients**
* All type of products must have **native price** and can have **sell price**
* All type of products can have measurement type: *g, kg, ml, litr or unit*
* Products must contain own availablity information per shop or global
* Product of the day/week. (OPTIONAL)

3. Orders
* Managers can view only last 2 days orders by shop
* Only admin can add a new order.

4. Statistic
* Only Administrator can view statistics:
* Products selling statistics
* Order statistics
* Statistics from separated shops
* Next month income statistics (OPTIONAL)

6. Settings
* Settings configurable from Administrator side only
* Send monthly/weekly/daily statistic by email(OPTIONAL)
* Set product of day in month/day. (OPTIONAL)

7. Notifications
* Notify Admin/Manager about Daily plan/target fail.
* Daily target loading tail for employer.
* Notify Customer about Product of the day3
* Notify when ingredient is low
* Send quote to *Inactive* customers, to remind about you. (OPTIONAL)

##Global things

* Make a place where users can give feedbacks
* Working calendar for employers
* Daily/Monthly place for targets, shops or global.
* In statistics, Check failing days (if daily plan not committed than this day fail, by percent)
* Pre Order functionality
* Export in PDF, EXCEL
* Send notification to customers.


#DB Strcutrure
* Users
* id
* first_name
* last_name
* password
* phone
* date_of_birth
* role
* permissions:string[]
* shop_id
* salary
* salary_type
* is_active
* ?history:Bson[]
* avatar
* last_activity: timestamp
* created_at
* settings (settings for notification)

* Products
* id
* name
* description
* available
* unit_type
* is_deleted
* shop_id
* category_id
* native_price
* sell_price
* create_at

* Orders (based on product id)
* id
* shop_id
* created_at
* unit_type

* Shop activity board
* id
* initiator_id
* initiator_full_name
* action_type (Take From constants)
* action_desc
* created_at

#1st Phase
* Product management
<!-- * Multilang application -->
* Only Admin/Manager/Employer login
* Order
* Daily Target management
* Web Application Dashboard
* Set salary type and percent amount
* Dramarkgh mutq/elq, orva abarot, orva mnacord,
* Advanced expenses
* Product / category / shop


List of permissions
ADD_STANDARD_USER, ADD_SUPER_USER, MODIFY_USER, DELETE_USER, BLOCK_USER    ADD_PRODUCT, MODIFY_PRODUCT, REMOVE_PRODUCT
ADD_ORDER, MODIFY_ORDER, DELETE_ORDER
ADD_DAILY_PLAN
MODIFY_CASHFLOW
ADD_ADVANCED_EXPENSES
