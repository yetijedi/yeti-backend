# Nest.js starter kit

## Contains

- Nest.js server with predefined set of modules (db, auth, config, notifications, type-mapper and etc.). Dockerized.
- Configured Nginx docker image (basic reverse-proxy setup)
- PostgreSQL docker image

Precise modules info to be set...
