import { Module, NestModule, MiddlewareConsumer, Inject, RequestMethod } from "@nestjs/common";
import { DbModule } from "./modules/db";
import { UserModule } from "./modules/api/user";
import { AuthModule } from "./modules/api/auth";
import { GlobalModule } from "./modules/global";
import { SharedModule } from "./modules/shared";
import { AppController } from "./app.controller";
import { RequestMiddleware } from "./http/middlewares/request.middleware";
import { Config, ConfigModule, ConfigDiToken } from "./modules/config";
import { CipherModule } from "./modules/shared/cipher";
import { LoggerModule } from "./modules/logger";
import { ProductModule } from "./modules/api/product";
import {CategoryModule} from "./modules/api/category";
import {OrderModule} from "./modules/api/order";
import {SalesPlanModule} from "./modules/api/sales-plan";
import {ShopModule} from "./modules/api/shop";
import { ExpenseModule } from "./modules/api/expense";
import { NotificationModule } from "./modules/notification";
import { WorkspaceModule } from "./modules/api/workspace";
import { CommonController } from "./modules/api/common.controller";
// import { CronModule } from "./modules/cron";

@Module({
  imports: [
      DbModule,
      AuthModule,
      UserModule,
      GlobalModule,
      ConfigModule,
      SharedModule,
      LoggerModule,
      CipherModule,
      ProductModule,
      CategoryModule,
      OrderModule,
      SalesPlanModule,
      ShopModule,
      ExpenseModule,
      NotificationModule,
      WorkspaceModule,
  ],
  controllers: [AppController, CommonController],
  providers: [],
})
export class ApplicationModule implements NestModule {
    public constructor(
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config,
    ) {}
    public configure(consumer: MiddlewareConsumer): void {
        if (this.config.app.logRequests) {
            consumer
                .apply(RequestMiddleware)
                .forRoutes({
                    path: "*",
                    method: RequestMethod.ALL,
                });
        }
    }
}
