import { Get, Controller } from "@nestjs/common";

@Controller("/healthCheck")
export class AppController {

    public constructor() { }

    @Get("")
    public get(): string {
        return "API IS HEALTHY";
    }
}
