import "bluebird-global";
import * as express from "express";

import { NestFactory } from "@nestjs/core";
import { INestApplication } from "@nestjs/common";

import * as helmet from "helmet";

import { getNamespace } from "./modules/shared/cls";
import { ApplicationModule } from "./app.module";
import { GlobalExceptionFilter } from "./http/filters";
import { LoggerDiToken, LoggerService, LoggerModule } from "./modules/logger";
import { CommonValidationPipe } from "./modules/shared/pipes/common-validation.pipe";
import * as cookieParser from "cookie-parser";
import { ExpressAdapter } from "@nestjs/platform-express";


async function bootstrap() {
    const server = express();
    server.disable("etag");

    const app = await NestFactory.create(
        ApplicationModule,
        new ExpressAdapter(server),
    );
    const logger: LoggerService = app.select(LoggerModule).get(LoggerDiToken.LOGGER);

    setClsMiddleware(app);
    app.use(helmet({
        hsts: false,
        hidePoweredBy: true,
    }));

    app.use(cookieParser());
    app.useGlobalFilters(
        new GlobalExceptionFilter(logger),
    );
    app.useGlobalPipes(
        new CommonValidationPipe(),
    );

    app.setGlobalPrefix("/api");
    app.enableCors({
        origin: ["http://localhost:3000", "http://localhost:3001"],
        credentials: true,
    });

    await app.listen(4102);
}
bootstrap();


function setClsMiddleware(app: INestApplication) {
    app.use((req, res, next) => {
        const cls = getNamespace();

        cls.bindEmitter(req);
        cls.bindEmitter(res);

        cls.run(() => next());
    });
}
