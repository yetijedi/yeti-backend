export const RESOURCE_METADATA_TOKEN = "resource";
export function Resource(resource: string): ClassDecorator {
    return (target: any) => {
        Reflect.defineMetadata(RESOURCE_METADATA_TOKEN, resource, target);
    };
}
