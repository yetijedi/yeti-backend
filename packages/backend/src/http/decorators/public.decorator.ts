import { SetMetadata } from "@nestjs/common";

export const PUBLIC_METADATA_TOKEN = "is_public";
export const Public = () => SetMetadata(PUBLIC_METADATA_TOKEN, true);
