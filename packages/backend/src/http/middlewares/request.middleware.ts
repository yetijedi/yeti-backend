import * as moment from "moment";
import { Injectable, NestMiddleware } from "@nestjs/common";

@Injectable()
export class RequestMiddleware implements NestMiddleware {
    use(req: Request, res, next) {
        const body = `${moment().format("M-DD HH:mm:ss")} [Request]:
            Url: ${req.url}
            Method: ${req.method}
            `;
        console.log(body); // TODO: use app logger, format log output
        next();
    }
}
