import { InternalServerErrorException } from "@nestjs/common";
import * as _ from "lodash";

export class EntityNotCreatedException extends InternalServerErrorException {
    constructor(fileLine: string, id?: number, message?: string) {
        super(`${_.upperFirst(fileLine)}: ${message || "Something going wrong"}`);
    }
}
