import { Reflector } from "@nestjs/core";
import { CanActivate, ExecutionContext, Inject, Injectable, UnauthorizedException } from "@nestjs/common";
import { PUBLIC_METADATA_TOKEN } from "../decorators";
import { AuthDiToken, CurrentUserService } from "../../modules/api/auth";

@Injectable()
export class AuthorizeGuard implements CanActivate {
    constructor(
        @Inject(AuthDiToken.CURRENT_USER_SERVICE) private readonly currentUserService: CurrentUserService,
        private readonly reflector: Reflector
    ) {
    }

    public canActivate(context: ExecutionContext): boolean {
        const handler = context.getHandler();
        const isPublic = this.reflector.get<boolean>(PUBLIC_METADATA_TOKEN, handler);

        if (!isPublic && !this.currentUserService.get()) {
            throw new UnauthorizedException();
        }
        return true;
    }
}
