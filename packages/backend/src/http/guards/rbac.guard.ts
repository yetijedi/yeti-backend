import { Reflector } from "@nestjs/core";
import {
    CanActivate,
    ExecutionContext,
    ForbiddenException,
    Inject,
    Injectable,
    UnauthorizedException
} from "@nestjs/common";
import { RESOURCE_METADATA_TOKEN } from "../decorators";
import { AuthDiToken, CurrentUserNotDefinedException, CurrentUserService } from "../../modules/api/auth";

@Injectable()
export class RBACGuard implements CanActivate {
    constructor(
        @Inject(AuthDiToken.CURRENT_USER_SERVICE) private readonly currentUserService: CurrentUserService,
        private readonly reflector: Reflector
    ) {}

    public canActivate(context: ExecutionContext): boolean {
        const request = context.switchToHttp().getRequest();
        const resource = this.reflector.get<string>(RESOURCE_METADATA_TOKEN, context.getClass());
        if (!resource) {
            return true;
        }
        const user = this.currentUserService.get();
        if (!user) {
            throw new UnauthorizedException();
        }

        const { permissions } = user;
        if (!permissions.hasOwnProperty(resource)) {
            throw new ForbiddenException();
        }

        // super access
        if (permissions[resource] === true) {
            return true;
        }

        return permissions[resource].indexOf(request.method) > -1;
    }
}


