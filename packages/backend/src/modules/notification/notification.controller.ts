import {
    Get,
    Controller,
    Inject,
    UseGuards,
} from "@nestjs/common";

import { AuthorizeGuard } from "../../http/guards";
import { NotificationDiToken, EmailNotificationService } from ".";


@Controller("/notification")
@UseGuards(AuthorizeGuard)
export class NotificationController {

    public constructor(
        @Inject(NotificationDiToken.EMAIL_NOTIFICATION_SERVICE) private readonly service: EmailNotificationService,
    ) { }
}
