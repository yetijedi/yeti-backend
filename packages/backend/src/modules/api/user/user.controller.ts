import {
    Get,
    Post,
    Put,
    Query,
    Controller,
    Inject,
    Body,
    Param,
    ParseIntPipe,
    Delete,
    UseGuards,
    HttpStatus,
    HttpCode,
    ValidationPipe,
} from "@nestjs/common";

import { UserCreateDto, UserDto } from "./dto";
import { UserDiToken } from "./user.di";
import { AuthorizeGuard } from "../../../http/guards";
import { UserService } from "./user.service";
import { Resource } from "../../../http/decorators";
import { RESOURCE_CODES } from "../../shared/constants/resources";
import { RBACGuard } from "../../../http/guards/rbac.guard";


@Controller("/users")
@Resource(RESOURCE_CODES.USER)
@UseGuards(AuthorizeGuard, RBACGuard)
export class UserController {

    public constructor(
        @Inject(UserDiToken.USER_SERVICE) private readonly userService: UserService,
    ) { }

    @Get("")
    public get(): Promise<UserDto[]> {
        return this.userService.get();
    }

    @Get("/:id")
    public getById(@Param("id", new ValidationPipe({ transform: true })) id: number): Promise<UserDto> {
        return this.userService.getById(id);
    }

    @Post("")
    public create(@Body() user: UserCreateDto): Promise<UserDto> {
        return this.userService.create(user);
    }

    @Put("/:id")
    public update(@Body() user: UserDto): Promise<UserDto> {
        return this.userService.update(user);
    }

    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete("/:id")
    public delete(@Param("id", new ParseIntPipe()) id: number): Promise<void> {
        return this.userService.delete(id);
    }
}
