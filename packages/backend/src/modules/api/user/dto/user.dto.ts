import { BaseDto } from "../../../../base.dto";
import * as _ from "lodash";
import { ShopDto } from "../../shop";
import { SalaryTypes } from "./user.enum";
import { IsBoolean, IsInt, IsOptional, IsString, IsIn, IsDate, ValidateIf, IsNotEmpty, IsEmail } from "class-validator";

export function getFullNameSafe(firstName: string = "", lastName: string = "") {
    return _.trim(`${firstName || ""} ${lastName || ""}`);
}

export class UserCreateDto extends BaseDto {
    @IsString()
    first_name: string;

    @IsString()
    last_name: string;

    @ValidateIf(o => o.email)
    @IsString()
    phone?: string;

    @ValidateIf(o => o.phone)
    @IsEmail()
    email?: string;

    @IsString()
    password: string;

    @IsInt()
    role_id: number;

    @IsInt()
    shop_id: number;

    @IsInt()
    @IsOptional()
    salary?: number;

    @IsOptional()
    @IsIn([SalaryTypes.PERCENT, SalaryTypes.FIXED])
    salary_type?: SalaryTypes;

    @IsBoolean()
    @IsOptional()
    is_active?: boolean;
}

export class UserDto extends UserCreateDto {
    @IsInt()
    id: number;

    role: any;
    shop: ShopDto;

    @IsDate()
    last_login: Date;
}

export class UserUpdateDto extends BaseDto {
    @IsInt()
    id: number;

    @IsString()
    @IsOptional()
    first_name?: string;

    @IsString()
    @IsOptional()
    last_name?: string;

    @IsString()
    @IsOptional()
    username?: string;

    @IsString()
    @IsOptional()
    phone?: string;

    @IsString()
    @IsOptional()
    password?: string;

    @IsInt()
    @IsOptional()
    role_id?: number;

    @IsInt()
    @IsOptional()
    shop_id?: number;

    @IsInt()
    @IsOptional()
    salary?: number;

    @IsOptional()
    @IsIn([SalaryTypes.PERCENT, SalaryTypes.FIXED])
    salary_type?: SalaryTypes;

    @IsOptional()
    @IsBoolean()
    is_active?: boolean;
}
