import { Injectable, Inject } from "@nestjs/common";

import { IFindOptions } from "sequelize-typescript";

import { UserCreateDto, UserDto } from "./dto";
import { BaseService } from "../../../base.service";
import { EntityNotFoundException } from "../../../http/exceptions";
import { DbDiToken, User } from "../../db";
import { TypeMapperDiToken, TypeMapper } from "../../shared";
import {Role} from "../../db/entities";

@Injectable()
export class UserService extends BaseService {

    withRoles: IFindOptions<User> = {
        include: [
            {
                model: Role,
                as: "role",
            },
        ],
    };

    public constructor(
        @Inject(DbDiToken.USER_REPOSITORY) private readonly repository: typeof User,
        @Inject(TypeMapperDiToken.MAPPER) private readonly typeMapper: TypeMapper,
    ) {
        super();
    }

    public async get(): Promise<UserDto[]> {
        const users = await this.repository.findAll(this.withRoles);
        return users.map((user: User) => this.typeMapper.map(User, UserDto, user.toJSON()));
    }

    public async updateLoginDate(id: number): Promise<void> {
        await this.updateBy<UserDto>(this.repository, User, { id, last_login: new Date() }, "id", false, ["last_login"]);
    }

    public async updatePasswordHash(user: { id: number, password: string }): Promise<void> {
        await this.updateBy<UserDto>(this.repository, User, user, "id", false, ["password"]);
    }

    public async getById(id: number, includeHash: boolean = false): Promise<UserDto> {
        const user: User = await this.repository.findByPk(id, this.withRoles);
        if (!user) {
            throw new EntityNotFoundException("User", id);
        }
        return this.typeMapper.map(User, UserDto, user, { includeHash });
    }

    public async getByPhoneEmail(username: string): Promise<UserDto> {
        const EMAIL_REGEXP = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}/g;
        const where: any = {};

        if (EMAIL_REGEXP.test(username)) {
            where.email = username;
        } else {
            where.phone = username;
        }

        const user: User = await this.repository.findOne({
            where,
            ...this.withRoles
        });

        if (!user) {
            return null;
        }

        return this.typeMapper.map(User, UserDto, user, { includeHash: true });
    }

    public async update(user: UserDto): Promise<UserDto> {
        // TODO: add security check if user edits himself!
        await Promise.all([
            super.updateBy(this.repository, User, user),
            // this.contactService.update(user.contact),
        ]);

        return this.getById(user.id);
    }

    public async create(user: UserCreateDto): Promise<UserDto> {
        return this.typeMapper.map(User, UserDto, await this.repository.create(user));
    }

    public async delete(id: number): Promise<void> {
        const user: User = await this.repository.findByPk(id);

        if (!user) {
            throw new EntityNotFoundException("User", id);
        }

        await user.destroy();
        // await this.contactService.delete(user.contactId);
    }
}
