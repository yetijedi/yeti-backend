import { UserDto } from "./dto";
import { TypeMapper } from "../../shared";
import { User } from "../../db";


export function register(mapper: TypeMapper) {
    mapper.register(User, UserDto, (user: User, opts) => {
        const _user = new UserDto({...user});
        if (!opts.includeHash) {
            delete _user.password;
        }
        return _user;
    });
}
