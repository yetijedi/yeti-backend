import { Module, NestModule, forwardRef, Inject } from "@nestjs/common";

import { DbModule } from "../../db";
import { AuthModule } from "../auth";
import { ConfigModule } from "../../config";
import { ShopController } from "./shop.controller";
import { register as registerTypeMappings } from "./shop.type-mappings";
import { SharedModule, TypeMapperDiToken, TypeMapper } from "../../shared";
import {ShopDiToken} from "./shop.di";
import {ShopService} from "./shop.service";
import { ProductModule } from "../product";
import { SalesPlanModule } from "../sales-plan";

const categoryProviders = [
    {
        provide: ShopDiToken.SHOP_SERVICE,
        useClass: ShopService,
    },
];

@Module({
    imports: [
        DbModule,
        ProductModule,
        SalesPlanModule,
        forwardRef(() => AuthModule),
        SharedModule,
        ConfigModule,
    ],
    controllers: [ShopController],
    providers: [...categoryProviders],
    exports: [...categoryProviders],
})
export class ShopModule implements NestModule {
    public constructor(
        @Inject(TypeMapperDiToken.MAPPER) private readonly mapper: TypeMapper,
    ) {
        registerTypeMappings(mapper);
    }

    public configure(): void {

    }
}
