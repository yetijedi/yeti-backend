import {
    Get,
    Post,
    Put,
    Controller,
    Inject,
    Body,
    Param,
    ParseIntPipe,
    Delete,
    UseGuards,
    HttpStatus,
    HttpCode,
    ValidationPipe,
} from "@nestjs/common";

import { ShopCreateDto, ShopDto } from "./shop.dto";
import {ShopDiToken} from "./shop.di";
import { AuthorizeGuard } from "../../../http/guards";
import { ShopService } from "./shop.service";
import { ProductDiToken, ProductDto, ProductService } from "../product";
import { VIRTUAL_SHOP_ID } from "../../db/seeder/shop";
import { SalesPlanDiToken, ShopSalesPlanService } from "../sales-plan";
import { RBACGuard } from "../../../http/guards/rbac.guard";
import { Resource } from "../../../http/decorators";
import { RESOURCE_CODES } from "../../shared/constants/resources";


@Controller("/shops")
@Resource(RESOURCE_CODES.SHOP)
@UseGuards(AuthorizeGuard, RBACGuard)
export class ShopController {

    public constructor(
        @Inject(SalesPlanDiToken.SHOP_SALES_PLAN_SERVICE) private readonly shopSalesPlanService: ShopSalesPlanService,
        @Inject(ShopDiToken.SHOP_SERVICE) private readonly shopService: ShopService,
        @Inject(ProductDiToken.PRODUCT_SERVICE) private readonly productService: ProductService,
    ) { }

    @Get("")
    public get(): Promise<ShopDto[]> {
        return this.shopService.get();
    }

    @Get("/:id")
    public getById(@Param("id", new ValidationPipe({ transform: true })) id: number): Promise<ShopDto> {
        return this.shopService.getById(id);
    }

    @Post("")
    public create(@Body() shop: ShopCreateDto): Promise<ShopDto> {
        return this.shopService.create(shop);
    }

    @Put("/:id")
    public update(@Body() shop: ShopDto): Promise<ShopDto> {
        return this.shopService.update(shop);
    }

    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete("/:id")
    public async delete(@Param("id", new ParseIntPipe()) id: number) {
        await this.shopService.delete(id);
        const products: ProductDto[] = await this.productService.get({ where: {shop_id: id} });
        products.map(product => {
            this.productService.update({
                ...product,
                shop_id: VIRTUAL_SHOP_ID
            });
        });

        await this.shopSalesPlanService.deleteByShopId(id);
    }
}
