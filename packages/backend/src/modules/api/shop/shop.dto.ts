import { BaseDto } from "../../../base.dto";
import { IsOptional, IsString, IsInt } from "class-validator";

export class ShopCreateDto extends BaseDto {
    @IsString()
    name: string;

    @IsString()
    @IsOptional()
    description?: string;

    @IsInt()
    working_days: number;

    @IsString()
    @IsOptional()
    open_at?: string;

    @IsString()
    @IsOptional()
    close_at?: string;

    @IsInt()
    current_balance: number;
}

export class ShopDto extends ShopCreateDto {
    @IsInt()
    id: number;
}

export class ShopUpdateDto extends BaseDto {
    @IsInt()
    id: number;

    @IsString()
    @IsOptional()
    name?: string;

    @IsString()
    @IsOptional()
    description?: string;

    @IsInt()
    @IsOptional()
    working_days?: number;

    @IsString()
    @IsOptional()
    open_at?: string;

    @IsString()
    @IsOptional()
    close_at?: string;

    @IsInt()
    @IsOptional()
    current_balance?: number;
}
