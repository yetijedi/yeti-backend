import { Injectable, Inject } from "@nestjs/common";

import { ShopCreateDto, ShopDto } from "./shop.dto";
import { BaseService } from "../../../base.service";
import { ConfigDiToken, Config } from "../../config";
import { EntityNotFoundException } from "../../../http/exceptions";
import { Shop, DbDiToken } from "../../db";
import { TypeMapperDiToken, TypeMapper } from "../../shared";
import { IFindOptions } from "sequelize-typescript";
import { OrderDto } from "../order";

@Injectable()
export class ShopService extends BaseService {

    public constructor(
        @Inject(DbDiToken.SHOP_REPOSITORY) private readonly repository: typeof Shop,
        @Inject(TypeMapperDiToken.MAPPER) private readonly typeMapper: TypeMapper,
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config
    ) {
        super();
    }

    public async get(filters: IFindOptions<OrderDto> = {}): Promise<ShopDto[]> {
        const shops = await this.repository.findAll(filters);
        return shops.map((shop: Shop) => this.typeMapper.map(Shop, ShopDto, shop.toJSON()));
    }

    public async getById(id: number): Promise<ShopDto> {
        const shop: Shop = await this.getEntityById(id);

        return this.typeMapper.map(Shop, ShopDto, shop);
    }

    public async update(shop: ShopDto): Promise<ShopDto> {
        await Promise.all([
            super.updateBy(this.repository, Shop, shop),
        ]);

        return this.typeMapper.map(Shop, ShopDto, await this.getEntityById(shop.id));
    }

    public async create(shop: ShopCreateDto): Promise<ShopDto> {
        const newShop = await this.repository.create(shop);
        return this.typeMapper.map(Shop, ShopDto, newShop.toJSON());
    }

    public async delete(id: number): Promise<void> {
        const shop: Shop = await this.repository.findByPk(id);

        if (!shop) {
            throw new EntityNotFoundException("Shop", id);
        }

        await shop.destroy();
    }

    public async updateShopBalance(shop_id: number, total: number, factor: number = 1): Promise<ShopDto> {
        const shop: Shop = await this.getEntityById(shop_id);
        shop.current_balance += factor * total;
        return this.update(shop);
    }

    private async getEntityById(id: number): Promise<Shop> {
        console.log(this.repository);
        const existingShopInstance: Shop = await this.repository.findByPk(id);

        if (!existingShopInstance) {
            throw new EntityNotFoundException("Shop", id);
        }

        return existingShopInstance.toJSON();
    }
}
