import { ShopDto } from "./shop.dto";
import { TypeMapper } from "../../shared";
import { Shop } from "../../db";


export function register(mapper: TypeMapper) {
    mapper.register(Shop, ShopDto, (shop: Shop) => {
        return new ShopDto({ ...shop });
    });
}
