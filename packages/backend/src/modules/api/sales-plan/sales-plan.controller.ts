import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Inject,
    Param,
    ParseIntPipe,
    Post,
    Put,
    UseGuards,
    ValidationPipe,
} from "@nestjs/common";

import { ShopSalesPlanPreCreateDto, ShopSalesPlanDto } from "./dto";
import { SalesPlanDiToken } from "./sales-plan.di";
import { AuthorizeGuard } from "../../../http/guards";
import { ShopSalesPlanService } from "./services";
import { IFindOptions } from "sequelize-typescript";
import { ShopSalesPlan } from "../../db";
import { SalesPlanStatuses } from "./dto";
import { Resource } from "../../../http/decorators";
import { RESOURCE_CODES } from "../../shared/constants/resources";
import { RBACGuard } from "../../../http/guards/rbac.guard";

@Controller("/sales-plan")
@Resource(RESOURCE_CODES.SALES_PLAN)
@UseGuards(AuthorizeGuard, RBACGuard)
export class SalesPlanController {

    public constructor(
        @Inject(SalesPlanDiToken.SHOP_SALES_PLAN_SERVICE) private readonly shopSalesPlanService: ShopSalesPlanService,
    ) { }

    @Get("")
    public get(): Promise<ShopSalesPlanDto[]> {
        return this.shopSalesPlanService.get();
    }

    @Get("/:id")
    public getById(@Param("id", new ValidationPipe({ transform: true })) id: number): Promise<ShopSalesPlanDto> {
        return this.shopSalesPlanService.getById(id);
    }

    @Post("")
    public create(@Body() salesPlan: ShopSalesPlanPreCreateDto): Promise<ShopSalesPlanDto> {
        return this.shopSalesPlanService.create(salesPlan);
    }

    @Put("/:id")
    public update(@Body() salesPlan: ShopSalesPlanDto): Promise<ShopSalesPlanDto> {
        return this.shopSalesPlanService.update(salesPlan);
    }

    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete("/:id")
    public async delete(@Param("id", new ParseIntPipe()) id: number): Promise<void> {
        const shopSalesPlans = await this.shopSalesPlanService.get({
            sales_plan_id: id,
            status: [SalesPlanStatuses.NOT_STARTED, SalesPlanStatuses.IN_PROGRESS]
            } as IFindOptions<ShopSalesPlan>
        );

        if (shopSalesPlans.length) {
            const queue = [];
            shopSalesPlans.forEach(shopSalesPlan => {
                const promise = this.shopSalesPlanService.update({
                    ...shopSalesPlan,
                    status: SalesPlanStatuses.FAIL
                });
                queue.push(promise);
            });
            await queue;
        }


        return this.shopSalesPlanService.delete(id);
    }
}
