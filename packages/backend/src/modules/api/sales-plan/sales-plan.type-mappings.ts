import { TypeMapper } from "../../shared";
import { SalesPlanDate, ShopSalesPlan } from "../../db";
import { ShopSalesPlanDto, SalesPlanDateDto } from "./dto";


export function register(mapper: TypeMapper) {
    mapper.register(ShopSalesPlan, ShopSalesPlanDto, (shopSalesPlan: ShopSalesPlan) => {
        return new ShopSalesPlanDto({ ...shopSalesPlan });
    });
    mapper.register(SalesPlanDate, SalesPlanDateDto, (salesPlanDate: SalesPlanDate) => {
        return new SalesPlanDateDto({ ...salesPlanDate });
    });
}
