import { Module, NestModule, forwardRef, Inject } from "@nestjs/common";

import { DbModule } from "../../db";
import { AuthModule } from "../auth";
import { ConfigModule } from "../../config";
import { SalesPlanController } from "./sales-plan.controller";
import { register as registerTypeMappings } from "./sales-plan.type-mappings";
import { SharedModule, TypeMapperDiToken, TypeMapper } from "../../shared";
import {SalesPlanDiToken} from "./sales-plan.di";
import { ShopSalesPlanService, SalesPlanDateService } from "./services";

const categoryProviders = [
    {
        provide: SalesPlanDiToken.SHOP_SALES_PLAN_SERVICE,
        useClass: ShopSalesPlanService,
    },
    {
        provide: SalesPlanDiToken.SALES_PLAN_DATE_SERVICE,
        useClass: SalesPlanDateService,
    },
];

@Module({
    imports: [
        DbModule,
        forwardRef(() => AuthModule),
        SharedModule,
        ConfigModule,
    ],
    controllers: [SalesPlanController],
    providers: [...categoryProviders],
    exports: [...categoryProviders],
})
export class SalesPlanModule implements NestModule {
    public constructor(
        @Inject(TypeMapperDiToken.MAPPER) private readonly mapper: TypeMapper,
    ) {
        registerTypeMappings(mapper);
    }

    public configure(): void {

    }
}
