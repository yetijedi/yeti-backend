import { BadRequestException, Inject, Injectable } from "@nestjs/common";

import { SalesPlanDateCreateDto, ShopSalesPlanDto, ShopSalesPlanPreCreateDto, SalesPlanDateDto } from "../dto";
import { BaseService } from "../../../../base.service";
import { Config, ConfigDiToken } from "../../../config";
import { EntityNotCreatedException, EntityNotFoundException } from "../../../../http/exceptions";
import { DbDiToken, SalesPlanDate, Shop, ShopSalesPlan } from "../../../db";
import { TypeMapper, TypeMapperDiToken } from "../../../shared";
import { IFindOptions } from "sequelize-typescript";
import { SalesPlanStatuses } from "../dto";
import * as _ from "lodash";
import * as moment from "moment";

@Injectable()
export class ShopSalesPlanService extends BaseService {
    withShop: IFindOptions<Shop[]> = {
        include: [
            {
                model: Shop,
                as: "shops",
            },
        ],
    };

    public constructor(
        @Inject(DbDiToken.SHOP_SALES_PLAN_REPOSITORY) private readonly repository: typeof ShopSalesPlan,
        @Inject(DbDiToken.SALES_PLAN_DATE) private readonly datesRepository: typeof SalesPlanDate,
        @Inject(TypeMapperDiToken.MAPPER) private readonly typeMapper: TypeMapper,
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config
    ) {
        super();
    }

    public async get(filter?: IFindOptions<ShopSalesPlan>): Promise<ShopSalesPlanDto[]> {
        const shopSalesPlans = await this.repository.findAll(filter || {});
        return shopSalesPlans.map((shopSalesPlan: ShopSalesPlan) => this.typeMapper.map(ShopSalesPlan, ShopSalesPlanDto, shopSalesPlan));
    }

    public async getById(id: number): Promise<ShopSalesPlanDto> {
        const shopSalesPlan: ShopSalesPlan = await this.getEntityById(id);
        return this.typeMapper.map(ShopSalesPlan, ShopSalesPlanDto, shopSalesPlan);
    }

    public async update(shopSalesPlan: ShopSalesPlanDto): Promise<ShopSalesPlanDto> {
        await super.updateBy(this.repository, ShopSalesPlan, shopSalesPlan);
        return this.typeMapper.map(ShopSalesPlan, ShopSalesPlanDto, await this.getEntityById(shopSalesPlan.id));
    }

    public async create(shopSalesPlan: ShopSalesPlanPreCreateDto): Promise<ShopSalesPlanDto> {
        let startDate = shopSalesPlan.start_date
            , endDate = shopSalesPlan.end_date
            , datesObjectives = shopSalesPlan.dates ? [...shopSalesPlan.dates] : [];

        if (!startDate || !endDate) {
            if (!datesObjectives.length) {
                throw new BadRequestException("Please specify start date, end date or custom dates");
            }

            const totalSuccessPoint = shopSalesPlan.dates.reduce((acc, cur) => acc + cur.success_point, 0);
            if (totalSuccessPoint !== shopSalesPlan.total_success_point) {
                throw new BadRequestException("Wrong total number, please check it again or contact with support");
            }

            delete shopSalesPlan.dates;

            datesObjectives = datesObjectives
                .map(date => ({
                    ...date,
                    date: new Date(date.date)
                }))
                .sort((d, b) => +d.date - +b.date);

            startDate = _.first(datesObjectives).date;
            endDate = _.last(datesObjectives).date;
        }

        const newShopSales = await this.repository.create({
            ...shopSalesPlan,
            start_date: startDate,
            end_date: endDate,
            status: SalesPlanStatuses.NOT_STARTED,
            shop_progress: 0
        });
        if (!newShopSales) {
            throw new EntityNotCreatedException("SalesPlanController");
        }

        const dates = [];
        if (datesObjectives.length) {
            const datesLn = datesObjectives.length;
            for (let i = 0; i < datesLn; i++) {
                dates.push({
                    shop_sales_plan_id: newShopSales.id,
                    date: datesObjectives[i].date,
                    status: SalesPlanStatuses.NOT_STARTED,
                    success_point: shopSalesPlan.autoCalc
                        ? Math.floor(shopSalesPlan.total_success_point / datesLn)
                        : datesObjectives[i].success_point,
                    progress: 0
                });
            }
        }
        else {
            const diff = moment(endDate).diff(moment(startDate), "days") + 1; // 1 day passed
            for (let i = 0; i < diff; i++) {
                dates.push({
                    shop_sales_plan_id: newShopSales.id,
                    date: moment(startDate).add(i, "days").toDate(),
                    status: SalesPlanStatuses.NOT_STARTED,
                    success_point: Math.floor(shopSalesPlan.total_success_point / diff),
                    progress: 0
                });
            }
        }

        const newDates = await this.createDates(dates);
        if (!newDates.length) {
            await this.delete(newShopSales.id);
            throw new EntityNotCreatedException("SalesPlanController");
        }

        return this.typeMapper.map(ShopSalesPlan, ShopSalesPlanDto, {
            ...newShopSales.toJSON(),
            dates: newDates
        });
    }

    public async delete(id: number): Promise<void> {
        const shopSalesPlan: ShopSalesPlan = await this.getEntityById(id);
        await shopSalesPlan.destroy();
    }

    public async deleteByShopId(id: number): Promise<void> {
        const shopSalesPlan = await this.repository.findOne({
            shop_id: id,
            status: [SalesPlanStatuses.IN_PROGRESS, SalesPlanStatuses.NOT_STARTED]
        } as IFindOptions<ShopSalesPlan>);

        if (!shopSalesPlan) {
          return null;
        }
        await shopSalesPlan.destroy();
    }

    public async getPlanByShopId(shopId: number): Promise<ShopSalesPlanDto> {
        const filter: IFindOptions<ShopSalesPlan> = {
            where: {
                shop_id: shopId,
                status: [SalesPlanStatuses.NOT_STARTED, SalesPlanStatuses.IN_PROGRESS]
            }
        };

        const shopPlan: ShopSalesPlan = await this.repository.findOne(filter);
        if (!shopPlan) {
            return null;
        }
        return this.typeMapper.map(ShopSalesPlan, ShopSalesPlanDto, shopPlan);
    }

    public async updateShopProgress(shopId: number, amount: number, existingShopPlan?: ShopSalesPlanDto): Promise<ShopSalesPlanDto> {
        const shopPlan: ShopSalesPlanDto = existingShopPlan || await this.getPlanByShopId(shopId);
        if (!shopPlan) {
            return null;
        }

        const { total_success_point } = shopPlan;
        const alpha = 100 / total_success_point * 100;

        shopPlan.shop_progress = shopPlan.shop_progress + alpha;
        if (shopPlan.shop_progress >= 100) {
            shopPlan.status = SalesPlanStatuses.SUCCESS;
        }

        return this.update(shopPlan);
    }

    private async createDates(dates: SalesPlanDateCreateDto[]): Promise<SalesPlanDateDto[]>{
        const _dates = await this.datesRepository.bulkCreate(dates, { returning: true });
        return _dates.map(d => this.typeMapper.map(SalesPlanDate, SalesPlanDateDto, d));
    }

    private async getEntityById(id: number): Promise<ShopSalesPlan> {
        const existingSalesPlanInstance: ShopSalesPlan = await this.repository.findByPk(id);

        if (!existingSalesPlanInstance) {
            throw new EntityNotFoundException("SalesPlan", id);
        }

        return existingSalesPlanInstance;
    }
}
