import { Inject, Injectable } from "@nestjs/common";
import { BaseService } from "../../../../base.service";
import { Config, ConfigDiToken } from "../../../config";
import { EntityNotFoundException } from "../../../../http/exceptions";
import { DbDiToken, SalesPlanDate } from "../../../db";
import { TypeMapper, TypeMapperDiToken } from "../../../shared";
import { IFindOptions } from "sequelize-typescript";
import { SalesPlanDateDto, SalesPlanStatuses } from "../dto";
import * as moment from "moment";

@Injectable()
export class SalesPlanDateService extends BaseService {

    public constructor(
        @Inject(DbDiToken.SALES_PLAN_DATE) private readonly repository: typeof SalesPlanDate,
        @Inject(TypeMapperDiToken.MAPPER) private readonly typeMapper: TypeMapper,
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config
    ) {
        super();
    }

    public async get(): Promise<SalesPlanDateDto[]> {
        const salesPlanDates = await this.repository.findAll();
        return salesPlanDates.map((salesPlanDate: SalesPlanDate) => this.typeMapper.map(SalesPlanDate, SalesPlanDateDto, salesPlanDate));
    }

    public async getById(id: number): Promise<SalesPlanDateDto> {
        const salesPlanDate: SalesPlanDate = await this.getEntityById(id);
        return this.typeMapper.map(SalesPlanDate, SalesPlanDateDto, salesPlanDate);
    }

    public async update(salesPlanDate: SalesPlanDateDto): Promise<SalesPlanDateDto> {
        await super.updateBy(this.repository, SalesPlanDate, salesPlanDate);
        return this.typeMapper.map(SalesPlanDate, SalesPlanDateDto, await this.getEntityById(salesPlanDate.id));
    }

    public async create(salesPlanDate: SalesPlanDateDto): Promise<SalesPlanDateDto> {
        const newSalesPlanDate = await this.repository.create(salesPlanDate);
        return this.typeMapper.map(SalesPlanDate, SalesPlanDateDto, newSalesPlanDate.toJSON());
    }

    public async delete(id: number): Promise<void> {
        const salesPlanDate: SalesPlanDate = await this.getEntityById(id);
        await salesPlanDate.destroy();
    }

    public async updateDailyProgress(shopSalesPlanId: number, amount: number): Promise<number> {
        const filter: IFindOptions<SalesPlanDate> = {
            where: {
                shop_sales_plan_id: shopSalesPlanId,
                status: [SalesPlanStatuses.NOT_STARTED, SalesPlanStatuses.IN_PROGRESS],
                date: moment().toDate()
            }
        };

        const salesPlanDate: SalesPlanDate = await this.repository.findOne(filter);
        if (!salesPlanDate) {
            return null;
        }
        const dailyPlan: SalesPlanDateDto = salesPlanDate.toJSON();

        if (dailyPlan.status === SalesPlanStatuses.NOT_STARTED) {
            dailyPlan.status = SalesPlanStatuses.IN_PROGRESS;
        }

        const progress = dailyPlan.progress + (100 / dailyPlan.success_point * amount);
        dailyPlan.progress = progress;

        if (progress >= 100) {
            dailyPlan.progress = 100;
            dailyPlan.status = SalesPlanStatuses.SUCCESS;
        }

        await this.update(dailyPlan);
        return progress;
    }

    private async getEntityById(id: number): Promise<SalesPlanDate> {
        const existingSalesPlanDateInstance: SalesPlanDate = await this.repository.findByPk(id);

        if (!existingSalesPlanDateInstance) {
            throw new EntityNotFoundException("SalesPlan", id);
        }

        return existingSalesPlanDateInstance;
    }
}
