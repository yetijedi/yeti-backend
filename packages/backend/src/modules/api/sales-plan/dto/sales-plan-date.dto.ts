import { BaseDto } from "../../../../base.dto";
import { ShopSalesPlanDto } from "./shop-sales-plan.dto";
import { SalesPlanStatuses } from "./sales-plan.enum";
import { IsDateString, IsIn, IsInt, IsOptional, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

export class SalesPlanDatePreCreateDto extends BaseDto {
    @IsDateString()
    date: Date;

    @IsInt()
    success_point: number;
}

export class SalesPlanDateCreateDto extends BaseDto {
    @IsInt()
    shop_sales_plan_id: number;

    @IsDateString()
    date: Date;

    @IsInt()
    success_point: number;

    @IsIn([SalesPlanStatuses.SUCCESS, SalesPlanStatuses.IN_PROGRESS, SalesPlanStatuses.NOT_STARTED, SalesPlanStatuses.FAIL])
    status: SalesPlanStatuses;

    @IsInt()
    progress: number;
}

export class SalesPlanDateDto extends SalesPlanDateCreateDto {
    @IsInt()
    id: number;

    @ValidateNested({ each: true }) @Type(() => ShopSalesPlanDto)
    @IsOptional()
    shopSalesPlan?: ShopSalesPlanDto[];
}

export class SalesPlanDateUpdateDto extends BaseDto {
    @IsInt()
    id: number;

    @IsDateString()
    @IsOptional()
    date?: Date;

    @IsInt()
    @IsOptional()
    success_point?: number;

    @IsIn([SalesPlanStatuses.SUCCESS, SalesPlanStatuses.IN_PROGRESS, SalesPlanStatuses.NOT_STARTED, SalesPlanStatuses.FAIL])
    @IsOptional()
    status?: SalesPlanStatuses;

    @IsInt()
    @IsOptional()
    progress?: number;
}
