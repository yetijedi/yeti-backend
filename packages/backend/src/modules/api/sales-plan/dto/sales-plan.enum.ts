export enum SalesPlanStatuses {
    NOT_STARTED,
    IN_PROGRESS,
    SUCCESS,
    FAIL
}
