import { ShopDto } from "../../shop";
import { BaseDto } from "../../../../base.dto";
import { SalesPlanDateDto, SalesPlanDatePreCreateDto } from "./sales-plan-date.dto";
import { SalesPlanStatuses } from "./sales-plan.enum";
import { IsBoolean, IsDateString, IsIn, IsInt, IsOptional, IsString, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

export class ShopSalesPlanPreCreateDto extends BaseDto {
    @IsString()
    @IsOptional()
    name?: string;

    @IsString()
    @IsOptional()
    description?: string;

    @IsInt()
    shop_id: number;

    @IsBoolean()
    @IsOptional()
    autoCalc?: boolean;

    @IsInt()
    total_success_point: number;

    @ValidateNested({ each: true }) @Type(() => SalesPlanDatePreCreateDto)
    @IsOptional()
    dates?: SalesPlanDatePreCreateDto[];

    @IsDateString()
    @IsOptional()
    start_date?: Date;

    @IsDateString()
    @IsOptional()
    end_date?: Date;
}

export class ShopSalesPlanCreateDto extends BaseDto {
    @IsString()
    @IsOptional()
    name: string;

    @IsString()
    @IsOptional()
    description?: string;

    @IsInt()
    shop_id: number;

    @IsIn([SalesPlanStatuses.SUCCESS, SalesPlanStatuses.IN_PROGRESS, SalesPlanStatuses.NOT_STARTED, SalesPlanStatuses.FAIL])
    status: SalesPlanStatuses;

    @IsInt()
    total_success_point: number;

    @IsInt()
    shop_progress: number;

    @IsDateString()
    start_date: Date;

    @IsDateString()
    end_date: Date;
}

export class ShopSalesPlanDto extends ShopSalesPlanPreCreateDto {
    @IsInt()
    id: number;

    @IsDateString()
    start_date: Date;

    @IsDateString()
    end_date: Date;

    @IsInt()
    @IsOptional()
    shop_progress?: number;

    @IsIn([SalesPlanStatuses.SUCCESS, SalesPlanStatuses.IN_PROGRESS, SalesPlanStatuses.NOT_STARTED, SalesPlanStatuses.FAIL])
    status: SalesPlanStatuses;

    // Circular dependency
    // @ValidateNested({ each: true }) @Type(() => ShopDto)
    @IsOptional()
    shops?: ShopDto[];

    @ValidateNested({ each: true }) @Type(() => SalesPlanDateDto)
    @IsOptional()
    dates?: SalesPlanDateDto[];
}
