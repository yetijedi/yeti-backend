import { ExpenseDto } from "./dto/expense.dto";
import { TypeMapper } from "../../shared";
import { Expense } from "../../db";


export function register(mapper: TypeMapper) {
    mapper.register(Expense, ExpenseDto, (expense: Expense) => {
        return new ExpenseDto({ ...expense });
    });
}
