import { Module, NestModule, forwardRef, Inject } from "@nestjs/common";

import { DbModule } from "../../db";
import { AuthModule } from "../auth";
import { ConfigModule } from "../../config";
import { ExpenseController } from "./expense.controller";
import { register as registerTypeMappings } from "./expense.type-mappings";
import { SharedModule, TypeMapperDiToken, TypeMapper } from "../../shared";
import { ExpenseDiToken } from "./expense.di";
import { ExpenseService } from "./expense.service";
import { ShopModule } from "../shop";
import { ProductModule } from "../product";
import { UserModule } from "../user";

const expenseProviders = [
    {
        provide: ExpenseDiToken.EXPENSE_SERVICE,
        useClass: ExpenseService,
    },
];

@Module({
    imports: [
        DbModule,
        forwardRef(() => AuthModule),
        SharedModule,
        ConfigModule,
        ProductModule,
        UserModule,
        ShopModule
    ],
    controllers: [ExpenseController],
    providers: [...expenseProviders],
    exports: [...expenseProviders],
})
export class ExpenseModule implements NestModule {
    public constructor(
        @Inject(TypeMapperDiToken.MAPPER) private readonly mapper: TypeMapper,
    ) {
        registerTypeMappings(mapper);
    }

    public configure(): void {

    }
}
