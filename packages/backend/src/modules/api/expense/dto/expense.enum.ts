export enum ExpenseTypes {
    ADD_PRODUCT = "add_product",
    ADVANCED = "advanced",
    SALARY = "salary",
    WITHDRAWAL = "withdrawal"
}
