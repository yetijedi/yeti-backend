import { BaseDto } from "../../../../base.dto";
import { ExpenseTypes } from "./expense.enum";
import { IsIn, IsInt, IsOptional } from "class-validator";

export class ExpenseCreateDto extends BaseDto {
    @IsInt()
    shop_id: number;

    @IsInt()
    amount: number;

    @IsIn([ExpenseTypes.SALARY, ExpenseTypes.ADD_PRODUCT, ExpenseTypes.ADVANCED])
    type: ExpenseTypes;

    @IsInt()
    @IsOptional()
    product_id?: number;

    @IsInt()
    @IsOptional()
    user_id?: number;
}

export class ExpenseUpdateDto extends BaseDto {
    @IsInt()
    id: number;

    @IsInt()
    shop_id: number;

    @IsInt()
    @IsOptional()
    amount?: number;

    @IsIn([ExpenseTypes.SALARY, ExpenseTypes.ADD_PRODUCT, ExpenseTypes.ADVANCED])
    @IsOptional()
    type?: ExpenseTypes;

    @IsInt()
    @IsOptional()
    product_id?: number;

    @IsInt()
    @IsOptional()
    user_id?: number;
}

export class ExpenseDto extends ExpenseCreateDto {
    @IsInt()
    id: number;
}
