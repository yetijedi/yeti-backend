import { Injectable, Inject } from "@nestjs/common";

import { ExpenseCreateDto, ExpenseDto } from "./dto/expense.dto";
import { BaseService } from "../../../base.service";
import { ConfigDiToken, Config } from "../../config";
import { EntityNotFoundException } from "../../../http/exceptions";
import { DbDiToken, Expense } from "../../db";
import { TypeMapperDiToken, TypeMapper } from "../../shared";

@Injectable()
export class ExpenseService extends BaseService {

    public constructor(
        @Inject(DbDiToken.EXPENSE_REPOSITORY) private readonly repository: typeof Expense,
        @Inject(TypeMapperDiToken.MAPPER) private readonly typeMapper: TypeMapper,
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config
    ) {
        super();
    }

    public async get(): Promise<ExpenseDto[]> {
        const expenses = await this.repository.findAll();
        return expenses.map((expense: Expense) => this.typeMapper.map(Expense, ExpenseDto, expense.toJSON()));
    }

    public async getById(id: number): Promise<ExpenseDto> {
        const expense: Expense = await this.getEntityById(id);

        return this.typeMapper.map(Expense, ExpenseDto, expense);
    }

    public async update(expense: ExpenseDto): Promise<ExpenseDto> {
        await Promise.all([
            super.updateBy(this.repository, Expense, expense),
        ]);

        return this.typeMapper.map(Expense, ExpenseDto, await this.getEntityById(expense.id));
    }

    public async create(expense: ExpenseCreateDto): Promise<ExpenseDto> {
        const newExpense = await this.repository.create(expense);
        return this.typeMapper.map(Expense, ExpenseDto, newExpense.toJSON());
    }

    public async delete(id: number): Promise<void> {
        const expense: Expense = await this.repository.findByPk(id);

        if (!expense) {
            throw new EntityNotFoundException("Expense", id);
        }

        await expense.destroy();
    }

    private async getEntityById(id: number): Promise<Expense> {
        const existingExpenseInstance: Expense = await this.repository.findByPk(id);

        if (!existingExpenseInstance) {
            throw new EntityNotFoundException("Expense", id);
        }

        return existingExpenseInstance.toJSON();
    }
}
