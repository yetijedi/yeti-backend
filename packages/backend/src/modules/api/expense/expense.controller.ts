import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Inject,
    Param,
    ParseIntPipe,
    Post,
    Put,
    UseGuards,
    ValidationPipe,
} from "@nestjs/common";

import { ExpenseCreateDto, ExpenseDto } from "./dto/expense.dto";
import { ExpenseDiToken } from "./expense.di";
import { AuthorizeGuard } from "../../../http/guards";
import { ExpenseService } from "./expense.service";
import { ShopDiToken, ShopService } from "../shop";
import { ProductDiToken, ProductService } from "../product";
import { UserDiToken, UserService } from "../user";
import { ExpenseTypes } from "./dto";
import { Resource } from "../../../http/decorators";
import { RESOURCE_CODES } from "../../shared/constants/resources";
import { RBACGuard } from "../../../http/guards/rbac.guard";

@Controller("/expenses")
@Resource(RESOURCE_CODES.EXPENSE)
@UseGuards(AuthorizeGuard, RBACGuard)
export class ExpenseController {

    public constructor(
        @Inject(ExpenseDiToken.EXPENSE_SERVICE) private readonly expenseService: ExpenseService,
        @Inject(ShopDiToken.SHOP_SERVICE) private readonly shopService: ShopService,
        @Inject(ProductDiToken.PRODUCT_SERVICE) private readonly productService: ProductService,
        @Inject(UserDiToken.USER_SERVICE) private readonly userService: UserService,
    ) { }

    @Get("")
    public get(): Promise<ExpenseDto[]> {
        return this.expenseService.get();
    }

    @Get("/:id")
    public getById(@Param("id", new ValidationPipe({ transform: true })) id: number): Promise<ExpenseDto> {
        return this.expenseService.getById(id);
    }

    @Post("")
    public async create(@Body() expense: ExpenseCreateDto): Promise<ExpenseDto> {
        const newExpense = await this.expenseService.create(expense);
        if (newExpense.type === ExpenseTypes.ADD_PRODUCT && newExpense.product_id) {
            const product = await this.productService.getById(newExpense.product_id);
            if (!product) {
                console.log("Product was not found!");
            }
            const prod = {
                ...product,
                available: product.available + newExpense.amount
            };
            await this.productService.update(prod);
        } else if (newExpense.type === ExpenseTypes.SALARY && newExpense.user_id) {
            const user = await this.userService.getById(newExpense.user_id);
            if (!user) {
                console.log("User was not found!");
            }
            // @TODO: add user-salary table to track employers
        }

        await this.shopService.updateShopBalance(newExpense.shop_id, newExpense.amount, -1);
        return newExpense;
    }

    @Put("/:id")
    public update(@Body() expense: ExpenseDto): Promise<ExpenseDto> {
        return this.expenseService.update(expense);
    }

    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete("/:id")
    public delete(@Param("id", new ParseIntPipe()) id: number): Promise<void> {
        return this.expenseService.delete(id);
    }
}
