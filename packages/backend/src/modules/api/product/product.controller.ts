import {
    Get,
    Post,
    Put,
    Controller,
    Inject,
    Body,
    Param,
    ParseIntPipe,
    Delete,
    UseGuards,
    HttpStatus,
    HttpCode,
    ValidationPipe,
} from "@nestjs/common";

import { ProductCreateDto, ProductDto } from "./dto/product.dto";
import { ProductDiToken } from "./product.di";
import { AuthorizeGuard } from "../../../http/guards";
import { ProductService } from "./product.service";
import { Resource } from "../../../http/decorators";
import { RESOURCE_CODES } from "../../shared/constants/resources";
import { RBACGuard } from "../../../http/guards/rbac.guard";

@Controller("/products")
@Resource(RESOURCE_CODES.PRODUCT)
@UseGuards(AuthorizeGuard, RBACGuard)
export class ProductController {

    public constructor(
        @Inject(ProductDiToken.PRODUCT_SERVICE) private readonly productService: ProductService,
    ) { }

    @Get("")
    public get(): Promise<ProductDto[]> {
        return this.productService.get();
    }

    @Get("/:id")
    public getById(@Param("id", new ValidationPipe({ transform: true })) id: number): Promise<ProductDto> {
        return this.productService.getById(id);
    }

    @Post("")
    public create(@Body() product: ProductCreateDto): Promise<ProductDto> {
        return this.productService.create(product);
    }

    @Put("/:id")
    public update(@Body() product: ProductDto): Promise<ProductDto> {
        return this.productService.update(product);
    }

    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete("/:id")
    public async delete(@Param("id", new ParseIntPipe()) id: number): Promise<void> {
        await this.productService.delete(id);
        return this.productService.deleteReceipt(id);

    }
}
