import { BaseDto } from "../../../../base.dto";
import { CategoryDto } from "../../category";
import { ShopDto } from "../../shop";
import { IngredientsDto } from "./ingredients.dto";
import { IsInt, IsOptional, IsString, ValidateNested } from "class-validator";
import { Type } from "class-transformer";

export class ProductCreateDto extends BaseDto {
    @IsString()
    readonly name: string;

    @IsString()
    @IsOptional()
    readonly description?: string;

    @IsInt()
    readonly available: number;

    @IsInt()
    unit_type_id: number;

    @IsInt()
    readonly shop_id: number;

    @IsInt()
    @IsOptional()
    readonly category_id?: number;

    @IsInt()
    readonly native_price: number;

    @IsInt()
    @IsOptional()
    readonly sell_price?: number;

    @ValidateNested({ each: true }) @Type(() => IngredientsDto)
    @IsOptional()
    readonly ingredients?: IngredientsDto[];
}

export class ProductUpdateDto extends BaseDto {
    @IsInt()
    readonly id: number;

    @IsString()
    @IsOptional()
    readonly description?: string;

    @IsInt()
    @IsOptional()
    readonly available?: number;

    @IsInt()
    @IsOptional()
    readonly unit_type_id?: number;

    @IsInt()
    @IsOptional()
    readonly shop_id?: number;

    @IsInt()
    @IsOptional()
    readonly category_id?: number;

    @IsInt()
    @IsOptional()
    readonly native_price?: number;

    @IsInt()
    @IsOptional()
    readonly sell_price?: number;
}

export class ProductDto extends ProductCreateDto {
    @IsInt()
    readonly id: number;

    readonly unitType: any;

    readonly category: CategoryDto;

    readonly shop: ShopDto;

    @IsOptional()
    readonly ingredientProducts?: ProductDto;
}

