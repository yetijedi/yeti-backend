import { IsInt, IsString } from "class-validator";

export class IngredientsDto {
    @IsInt()
    readonly ingredient_product_id: number;

    @IsString()
    readonly name: string;

    @IsInt()
    readonly weight: number;

    @IsInt()
    readonly unit_type_id: number;

    @IsInt()
    readonly native_price: number;

    @IsInt()
    readonly category_id: number;

    @IsString()
    readonly category_name: string;
}
