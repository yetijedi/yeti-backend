import { Injectable, Inject, InternalServerErrorException, BadRequestException } from "@nestjs/common";

import { ProductDto, ProductCreateDto } from "./dto/product.dto";
import { BaseService } from "../../../base.service";
import { ConfigDiToken, Config } from "../../config";
import { EntityNotFoundException } from "../../../http/exceptions";
import { DbDiToken, Product, ReceiptedProduct, Role } from "../../db";
import { TypeMapperDiToken, TypeMapper } from "../../shared";
import { OrderItemDto } from "../order/dto/order-item.dto";
import { IngredientsDto } from "./dto/ingredients.dto";
import { IFindOptions } from "sequelize-typescript";
import { DEFAULT_CATEGORY_ID } from "../../db/seeder/category";
import { CategoryDiToken, CategoryDto, CategoryService } from "../category";

@Injectable()
export class ProductService extends BaseService {

    withIngredients: IFindOptions<Product> = {
        include: [
            {
                model: ReceiptedProduct,
                as: "ingredients",
            },
        ]
    };

    withReceipts: IFindOptions<Product> = {
        include: [
            {
                model: ReceiptedProduct,
                as: "receipts",
            },
        ]
    };

    public constructor(
        @Inject(DbDiToken.PRODUCT_REPOSITORY) private readonly repository: typeof Product,
        @Inject(DbDiToken.RECEIPTED_PRODUCT_REPOSITORY) private readonly receiptsRepository: typeof ReceiptedProduct,
        @Inject(TypeMapperDiToken.MAPPER) private readonly typeMapper: TypeMapper,
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config,
        @Inject(CategoryDiToken.CATEGORY_SERVICE) private readonly categoryService: CategoryService
    ) {
        super();
    }

    public async get(filter?: IFindOptions<Product>): Promise<ProductDto[]> {
        const products: Product[] = await this.repository.findAll(filter || this.withIngredients);
        return products.map((product: Product) => this.typeMapper.map(Product, ProductDto, product.toJSON()));
    }

    public async getById(id: number): Promise<ProductDto> {
        const product: Product = await this.getEntityById(id);
        return this.typeMapper.map(Product, ProductDto, product);
    }

    public async update(product: ProductDto): Promise<ProductDto> {
        await this.updateBy(this.repository, Product, product);
        return this.typeMapper.map(Product, ProductDto, await this.getEntityById(product.id));
    }

    public async create(product: ProductCreateDto): Promise<ProductDto> {
        if (product.category_id !== DEFAULT_CATEGORY_ID && product.category_id && !product.unit_type_id) {
            const category: CategoryDto = await this.categoryService.getById(product.category_id);
            if (category) {
                product.unit_type_id = category.unit_type_id;
            }
        }

        const newProduct = await this.repository.create(product);
        return this.typeMapper.map(Product, ProductDto, newProduct.toJSON());
    }

    public async delete(id: number): Promise<void> {
        const product: Product = await this.repository.findByPk(id);
        if (!product) {
            throw new EntityNotFoundException("Product", id);
        }
        await product.destroy();
    }

    public async deleteReceipt(id: number): Promise<void> {
        await super.multipleDestroyBy(this.receiptsRepository, [id], "receipt_product_id");
        await super.multipleDestroyBy(this.receiptsRepository, [id], "ingredient_product_id");
    }

    public async calculateAvailabilityFromOrder(orderItems: OrderItemDto[]): Promise<ProductDto[]> {
        const prodIds = orderItems.map(orderItem => orderItem.product_id);
        const products: Product[] = await this.repository.findAll({
            ...this.withReceipts,
            where: {
                id: prodIds
            }
        } as IFindOptions<Product>);

        const soldProducts = {}; // id: amount
        products.forEach((product: Product) => {
            const orderItem = orderItems.find((item: OrderItemDto) => item.product_id === product.id);
            if (!orderItem) {
                console.log("Order Item product was not found!!!");
            }
            if (!soldProducts.hasOwnProperty(product.id)) {
                soldProducts[product.id] = 0;
            }
            soldProducts[product.id] += orderItem.quantity;

            if (product.receipts.length) {
                product.receipts.forEach(receipt => {
                    const { ingredient_product_id, weight } = receipt;
                    if (!soldProducts.hasOwnProperty(ingredient_product_id)) {
                        soldProducts[ingredient_product_id] = 0;
                    }
                    soldProducts[ingredient_product_id] += weight * orderItem.quantity;
                });
            }
        });

        const receiptsAndIngredients = await this.repository.findAll({
            where: {
                id: Object.keys(soldProducts)
            }
        });

        // IGNORING AVAILABILITY AND UPDATE IT
        const queue = receiptsAndIngredients.map(prod => prod.decrement("available", { by: soldProducts[prod.id] }));
        const updatedProducts = await Promise.all(queue);

        return updatedProducts.map((product: Product) => this.typeMapper.map(Product, ProductDto, product.toJSON()));
    }

    private async getEntityById(id: number): Promise<Product> {
        const existingProductInstance: Product = await this.repository.findByPk(id);
        if (!existingProductInstance) {
            throw new EntityNotFoundException("Product", id);
        }

        return existingProductInstance.toJSON();
    }
}
