import { BaseDto } from "../../../base.dto";
import { IsInt, IsOptional, IsString } from "class-validator";
import { ShopDto } from "../shop";
import { ProductDto } from "../product";
import { CategoryDto } from "../category/dto";

export class WorkspaceDto {
    shop: Partial<ShopDto>;

    @IsString()
    last_order_id: number;

    products: Partial<ProductDto>[];

    categories: Partial<CategoryDto>[];
}
