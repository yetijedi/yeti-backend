import { Injectable, Inject } from "@nestjs/common";
import { BaseService } from "../../../base.service";
import { Category } from "../../db";
import { TypeMapperDiToken, TypeMapper } from "../../shared";
import { OrderDiToken, OrderService } from "../order";
import { AuthDiToken, CurrentUserService } from "../auth";
import { ProductDiToken, ProductService } from "../product";
import { ShopDiToken, ShopService } from "../shop";
import { WorkspaceDto } from "./workspace.dto";
import { Op } from "sequelize";
import { VIRTUAL_SHOP_ID } from "../../db/seeder/shop";

@Injectable()
export class WorkspaceService extends BaseService {

    public constructor(
        @Inject(AuthDiToken.CURRENT_USER_SERVICE) private readonly currentUserService: CurrentUserService,
        @Inject(OrderDiToken.ORDER_SERVICE) private readonly orderService: OrderService,
        @Inject(ShopDiToken.SHOP_SERVICE) private readonly shopService: ShopService,
        @Inject(ProductDiToken.PRODUCT_SERVICE) private readonly productService: ProductService,
        @Inject(TypeMapperDiToken.MAPPER) private readonly typeMapper: TypeMapper,
    ) {
        super();
    }

    public async getWorkspace(): Promise<WorkspaceDto> {
        const user = this.currentUserService.get();
        const shopInfo = await this.shopService.getById(user.shop_id);

        let lastOrderId = 0;
        const orders = await this.orderService.get({
            where: {
                shop_id: user.shop_id
            }
        });

        if (orders.length) {
            lastOrderId = orders[orders.length - 1].id;
        }

        const sellProducts = await this.productService.get({
            include: [
                {
                    model: Category,
                    as: "category",
                },
            ],
            where: {
                [Op.or]:  [
                    {shop_id: user.shop_id},
                    {shop_id: VIRTUAL_SHOP_ID}
                ],
                sell_price: {
                    [Op.gt]: 0
                },
                available: {
                    [Op.gt]: 0
                }
            }
        });

        // @TODO:
        // HashMap is liner but we can implement the
        // https://ru.wikipedia.org/wiki/%D0%9D%D0%B0%D1%85%D0%BE%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5_%D1%86%D0%B8%D0%BA%D0%BB%D0%B0
        // for duplciate items
        const categoriesMap = {};
        const products = sellProducts.map(prod => {
            const { name, description, available, sell_price, category_id, category, shop_id, id } = prod;

            if (!categoriesMap[category_id]) {
                categoriesMap[category_id] = {
                    id: category.id,
                    name: category.name
                };
            }

            return {
                id,
                name,
                description,
                available,
                sell_price,
                category_id,
                shop_id,
            }
        });

        return {
            shop: {
                id: shopInfo.id,
                name: shopInfo.name
            },
            last_order_id: lastOrderId,
            products,
            categories: Object.values(categoriesMap)
        };
    }
}
