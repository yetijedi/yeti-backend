import { Module, NestModule, forwardRef, Inject } from "@nestjs/common";

import { DbModule } from "../../db";
import { AuthModule } from "../auth";
import { ConfigModule } from "../../config";
import { WorkspaceController } from "./workspace.controller";
import { SharedModule } from "../../shared";
import { WorkspaceDiToken } from "./workspace.di";
import { WorkspaceService } from "./workspace.service";
import { ProductModule } from "../product";
import { SalesPlanModule } from "../sales-plan";
import { CategoryModule } from "../category";
import { ShopModule } from "../shop";
import { OrderModule } from "../order";

const workspaceProviders = [
    {
        provide: WorkspaceDiToken.WORKSPACE_SERVICE,
        useClass: WorkspaceService,
    },
];

@Module({
    imports: [
        forwardRef(() => AuthModule),
        ProductModule,
        CategoryModule,
        SalesPlanModule,
        OrderModule,
        ShopModule,
        SharedModule,
        ConfigModule,
    ],
    controllers: [WorkspaceController],
    providers: [...workspaceProviders],
    exports: [...workspaceProviders],
})
export class WorkspaceModule implements NestModule {
    public constructor() {
    }

    public configure(): void {

    }
}
