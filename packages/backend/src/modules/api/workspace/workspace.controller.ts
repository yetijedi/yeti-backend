import {
    Get,
    Post,
    Put,
    Controller,
    Inject,
    Body,
    Param,
    ParseIntPipe,
    Delete,
    UseGuards,
    HttpStatus,
    HttpCode,
    ValidationPipe,
} from "@nestjs/common";

import { WorkspaceDiToken } from "./workspace.di";
import { AuthorizeGuard } from "../../../http/guards";
import { WorkspaceService } from "./workspace.service";
import { WorkspaceDto } from "./workspace.dto";

@Controller("/workspace")
@UseGuards(AuthorizeGuard)
export class WorkspaceController {

    public constructor(
        @Inject(WorkspaceDiToken.WORKSPACE_SERVICE) private readonly service: WorkspaceService,
    ) {
    }

    @Get("")
    public get(): Promise<WorkspaceDto> {
        return this.service.getWorkspace();
    }
}
