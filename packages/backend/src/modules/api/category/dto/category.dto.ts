import { BaseDto } from "../../../../base.dto";
import { IsString, IsInt, IsOptional, IsIn } from "class-validator";

export class CategoryCreateDto extends BaseDto {
    @IsString()
    readonly name: string;

    @IsString()
    @IsOptional()
    readonly description?: string;

    @IsInt()
    readonly unit_type_id: number;
}

export class CategoryUpdateDto extends BaseDto {
    @IsInt()
    readonly id: number;

    @IsString()
    @IsOptional()
    readonly name?: string;

    @IsInt()
    @IsOptional()
    readonly unit_type_id?: number;
}

export class CategoryDto extends CategoryCreateDto {
    @IsInt()
    readonly id: number;
}
