import {
    Get,
    Post,
    Put,
    Controller,
    Inject,
    Body,
    Param,
    ParseIntPipe,
    Delete,
    UseGuards,
    HttpStatus,
    HttpCode,
    ValidationPipe,
} from "@nestjs/common";

import { CategoryCreateDto, CategoryDto } from "./dto";
import { CategoryDiToken } from "./category.di";
import { AuthorizeGuard } from "../../../http/guards";
import { CategoryService } from "./category.service";
import { RBACGuard } from "../../../http/guards/rbac.guard";
import { Resource } from "../../../http/decorators";
import { RESOURCE_CODES } from "../../shared/constants/resources";

@Controller("/categories")
@Resource(RESOURCE_CODES.CATEGORY)
@UseGuards(AuthorizeGuard, RBACGuard)
export class CategoryController {

    public constructor(
        @Inject(CategoryDiToken.CATEGORY_SERVICE) private readonly categoryService: CategoryService,
    ) { }

    @Get("")
    public get(): Promise<CategoryDto[]> {
        return this.categoryService.get();
    }

    @Get("/:id")
    public getById(@Param("id", new ValidationPipe({ transform: true })) id: number): Promise<CategoryDto> {
        return this.categoryService.getById(id);
    }

    @Post("")
    public create(@Body() category: CategoryCreateDto): Promise<CategoryDto> {
        return this.categoryService.create(category);
    }

    @Put("/:id")
    public update(@Body() category: CategoryDto): Promise<CategoryDto> {
        return this.categoryService.update(category);
    }

    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete("/:id")
    public delete(@Param("id", new ParseIntPipe()) id: number): Promise<void> {
        return this.categoryService.delete(id);
    }
}
