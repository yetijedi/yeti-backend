import { Module, NestModule, forwardRef, Inject } from "@nestjs/common";

import { DbModule } from "../../db";
import { AuthModule } from "../auth";
import { ConfigModule } from "../../config";
import { OrderController } from "./order.controller";
import { register as registerTypeMappings } from "./order.type-mappings";
import { SharedModule, TypeMapperDiToken, TypeMapper } from "../../shared";
import {OrderDiToken} from "./order.di";
import {OrderService} from "./order.service";
import { ShopModule } from "../shop";
import { SalesPlanModule } from "../sales-plan";
import { ProductModule } from "../product";

const orderProviders = [
    {
        provide: OrderDiToken.ORDER_SERVICE,
        useClass: OrderService,
    },
];

@Module({
    imports: [
        DbModule,
        forwardRef(() => AuthModule),
        SharedModule,
        ConfigModule,
        ShopModule,
        ProductModule,
        SalesPlanModule
    ],
    controllers: [OrderController],
    providers: [...orderProviders],
    exports: [...orderProviders],
})
export class OrderModule implements NestModule {
    public constructor(
        @Inject(TypeMapperDiToken.MAPPER) private readonly mapper: TypeMapper,
    ) {
        registerTypeMappings(mapper);
    }

    public configure(): void {
    }
}
