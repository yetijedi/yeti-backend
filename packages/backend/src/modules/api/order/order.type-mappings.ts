import { OrderDto } from "./dto/order.dto";
import { TypeMapper } from "../../shared";
import { Order } from "../../db";
import { OrderItem } from "../../db/entities/order-item.entity";
import { OrderItemDto } from "./dto/order-item.dto";


export function register(mapper: TypeMapper) {
    mapper.register(Order, OrderDto, (order: Order) => {
        return new OrderDto({ ...order });
    });
    mapper.register(OrderItem, OrderItemDto, (orderItem: OrderItem, opts) => {
        if (opts.asJsonB) {
            delete orderItem.created_at;
            delete orderItem.updated_at;
        }
        return new OrderItemDto({ ...orderItem });
    });
}
