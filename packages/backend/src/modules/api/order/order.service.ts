import { Injectable, Inject } from "@nestjs/common";

import { OrderCreateDto, OrderDto } from "./dto/order.dto";
import { BaseService } from "../../../base.service";
import { ConfigDiToken, Config } from "../../config";
import { EntityNotCreatedException, EntityNotFoundException } from "../../../http/exceptions";
import { Order, DbDiToken, Product, ReceiptedProduct } from "../../db";
import { TypeMapperDiToken, TypeMapper } from "../../shared";
import { OrderItemCreateDto, OrderItemDto } from "./dto/order-item.dto";
import { OrderItem } from "../../db/entities/order-item.entity";
import * as _ from "lodash";
import { IngredientsDto } from "../product/dto/ingredients.dto";
import { ProductDiToken, ProductDto, ProductService } from "../product";
import { IFindOptions } from "sequelize-typescript";
import { CategoryDto } from "../category/dto";
import { Op } from "sequelize";

@Injectable()
export class OrderService extends BaseService {

    public constructor(
        @Inject(DbDiToken.ORDER_REPOSITORY) private readonly repository: typeof Order,
        @Inject(DbDiToken.ORDER_ITEM_REPOSITORY) private readonly orderItemRepository: typeof OrderItem,
        @Inject(ProductDiToken.PRODUCT_SERVICE) private readonly productService: ProductService,
        @Inject(TypeMapperDiToken.MAPPER) private readonly typeMapper: TypeMapper,
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config
    ) {
        super();
    }

    public async get(filters: IFindOptions<OrderDto> = {}): Promise<OrderDto[]> {
        const orders = await this.repository.findAll(filters);
        return orders.map((order: Order) => this.typeMapper.map(Order, OrderDto, order.toJSON()));
    }

    public async getById(id: number): Promise<OrderDto> {
        const order: Order = await this.getEntityById(id);

        return this.typeMapper.map(Order, OrderDto, order);
    }

    public async update(order: OrderDto): Promise<OrderDto> {
        await super.updateBy(this.repository, Order, order);
        return this.typeMapper.map(Order, OrderDto, await this.getEntityById(order.id));
    }

    public async create(order: OrderCreateDto): Promise<OrderDto> {
        const newOrder: Order = await this.repository.create({
            ...order,
            total_amount: 0
        });

        if (!newOrder) {
            throw new EntityNotCreatedException("OrderController");
        }

        const newOrderObj: OrderDto = newOrder.toJSON();
        const orderItems = await this.createOrderItems(newOrderObj);

        //Rollback if its not created
        if (!orderItems) {
            await this.delete(newOrderObj.id);
            throw new EntityNotCreatedException("OrderController");
        }

        const total_amount = orderItems
            .map((orderItem: OrderItemDto) => orderItem.quantity * orderItem.sell_price)
            .reduce((prev, current) => prev + current, 0);

        return await this.update({
            ...newOrderObj,
            total_amount,
            order_info: orderItems.map(orderItem => ({
                ...this.typeMapper.map(OrderItem, OrderItemDto, orderItem.toJSON(), {asJsonB: true})
            }))
        });
    }

    public async delete(id: number): Promise<void> {
        const order: Order = await this.repository.findByPk(id);
        if (!order) {
            throw new EntityNotFoundException("Order", id);
        }
        await order.destroy();
    }

    public async deleteOrderItems(id: number, orderItemIds: number[]): Promise<void> {
        const order = await this.getEntityById(id);

        await super.multipleDestroyBy(this.orderItemRepository, orderItemIds);

        order.order_info = order.order_info.filter((orderItem: OrderItemDto) => orderItemIds.indexOf(orderItem.id) === -1);
        if (!order.order_info.length) {
            await this.delete(order.id);
        } else {
            await this.update(order);
        }
    }

    private async createOrderItems(order: OrderDto): Promise<OrderItem[]> {
        const groupedOrderItems = {};
        order.order_info.forEach((orderItem: OrderItemCreateDto) => {
            if (!groupedOrderItems.hasOwnProperty(orderItem.product_id)) {
                groupedOrderItems[orderItem.product_id] = {
                    quantity: 0
                };
            }
            groupedOrderItems[orderItem.product_id] = {
                ...orderItem,
                order_id: order.id,
                quantity: orderItem.quantity + groupedOrderItems[orderItem.product_id].quantity
            };
        });

        const prodIds = Object.keys(groupedOrderItems);
        const products = await this.productService.get({
            where: {
                id: {
                    [Op.in]: prodIds
                }
            }
        });

        products.forEach(product => {
           groupedOrderItems[product.id] = {
               ...groupedOrderItems[product.id],
               native_price: product.native_price,
               sell_price: product.sell_price,
               name: product.name
           };
        });
        return await this.orderItemRepository.bulkCreate(_.values(groupedOrderItems), {returning: true});
    }

    private async getEntityById(id: number): Promise<Order> {
        const existingOrderInstance: Order = await this.repository.findByPk(id);

        if (!existingOrderInstance) {
            throw new EntityNotFoundException("Order", id);
        }

        return existingOrderInstance.toJSON();
    }
}
