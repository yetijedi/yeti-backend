import {
    Get,
    Post,
    Put,
    Controller,
    Inject,
    Body,
    Param,
    ParseIntPipe,
    Delete,
    UseGuards,
    HttpStatus,
    HttpCode,
    ValidationPipe, Query,
} from "@nestjs/common";

import { OrderCreateDto, OrderDto } from "./dto/order.dto";
import { OrderDiToken } from "./order.di";
import { AuthorizeGuard } from "../../../http/guards";
import { OrderService } from "./order.service";
import { ShopDiToken, ShopService } from "../shop";
import { SalesPlanDateService, SalesPlanDiToken, ShopSalesPlanService } from "../sales-plan";
import { ProductDiToken, ProductService } from "../product";
import { ParseIntAsArrayPipe } from "../../shared/pipes";
import { RBACGuard } from "../../../http/guards/rbac.guard";
import { Resource } from "../../../http/decorators";
import { RESOURCE_CODES } from "../../shared/constants/resources";

@Controller("/orders")
@Resource(RESOURCE_CODES.ORDERS)
@UseGuards(AuthorizeGuard, RBACGuard)
export class OrderController {

    public constructor(
        @Inject(OrderDiToken.ORDER_SERVICE) private readonly orderService: OrderService,
        @Inject(ShopDiToken.SHOP_SERVICE) private readonly shopService: ShopService,
        @Inject(ProductDiToken.PRODUCT_SERVICE) private readonly productService: ProductService,
        @Inject(SalesPlanDiToken.SHOP_SALES_PLAN_SERVICE) private readonly shopSalesPlanService: ShopSalesPlanService,
        @Inject(SalesPlanDiToken.SALES_PLAN_DATE_SERVICE) private readonly salesPlanDateService: SalesPlanDateService,
    ) { }

    @Get("")
    public get(): Promise<OrderDto[]> {
        return this.orderService.get();
    }

    @Get("/:id")
    public getById(@Param("id", new ValidationPipe({ transform: true })) id: number): Promise<OrderDto> {
        return this.orderService.getById(id);
    }

    @Post("")
    public async create(@Body() order: OrderCreateDto) {
        /**
         * @NOTE: Calculate order total_amount
         * Create new single order and order items for each receipts (all order items are grouped by product_id)
         */
        const newOrder = await this.orderService.create(order);

        const { shop_id, total_amount, order_info } = newOrder;

        // @Note: Update product/ingredients availability count
        await this.productService.calculateAvailabilityFromOrder(order_info);

        // @Note: Update current shop balance
        await this.shopService.updateShopBalance(shop_id, total_amount);

        const shopPlan = await this.shopSalesPlanService.getPlanByShopId(shop_id);
        if (shopPlan) {
            // @Note: Update daily plan progress
            await this.salesPlanDateService.updateDailyProgress(shopPlan.id, total_amount);
            // @Note: Update shop plan progress
            await this.shopSalesPlanService.updateShopProgress(shop_id, total_amount, shopPlan);
        }

        return newOrder;
    }

    @Put("/:id")
    public update(@Body() shop: OrderDto): Promise<OrderDto> {
        return this.orderService.update(shop);
    }

    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete("/:id")
    public delete(@Param("id", new ParseIntPipe()) id: number): Promise<void> {
        return this.orderService.delete(id);
    }

    @HttpCode(HttpStatus.NO_CONTENT)
    @Delete("/:orderId/items")
    public deleteOrderItem(
        @Param("orderId", new ParseIntPipe()) orderId: number,
        @Query("ids", new ParseIntAsArrayPipe()) ids: number[],
    ): Promise<void> {
        return this.orderService.deleteOrderItems(orderId, ids);
    }
}
