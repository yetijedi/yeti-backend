import { BaseDto } from "../../../../base.dto";
import { IsInt, IsOptional, IsString } from "class-validator";

export class OrderItemPreCreateDto extends BaseDto {
    @IsInt()
    product_id: number;

    @IsInt()
    quantity: number;
}

export class OrderItemCreateDto extends BaseDto {
    @IsInt()
    product_id: number;

    @IsInt()
    quantity: number;

    @IsInt()
    native_price: number;

    @IsInt()
    sell_price: number;

    @IsString()
    name: string;
}

export class OrderItemUpdateDto extends BaseDto {
    @IsInt()
    id: number;

    @IsInt()
    order_id: number;

    @IsInt()
    @IsOptional()
    product_id?: number;

    @IsInt()
    @IsOptional()
    quantity?: number;

    @IsInt()
    @IsOptional()
    native_price?: number;

    @IsInt()
    @IsOptional()
    sell_price?: number;
}

export class OrderItemDto extends OrderItemCreateDto {
    @IsInt()
    id: number;

    @IsInt()
    order_id: number;

    @IsInt()
    native_price: number;

    @IsInt()
    sell_price: number;

    @IsString()
    name: string;
}
