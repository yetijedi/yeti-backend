import { BaseDto } from "../../../../base.dto";
import { OrderItemCreateDto, OrderItemDto, OrderItemPreCreateDto } from "./order-item.dto";
import { IsInt, IsOptional, ValidateNested } from "class-validator";
import { Type } from "class-transformer";


export class OrderCreateDto extends BaseDto {
    @IsInt()
    readonly shop_id: number;

    @ValidateNested({ each: true }) @Type(() => OrderItemPreCreateDto)
    order_info: OrderItemPreCreateDto[];
}

export class OrderUpdateDto extends BaseDto {
    @IsInt()
    readonly id: number;

    @IsInt()
    readonly shop_id: number;

    @ValidateNested({ each: true }) @Type(() => OrderItemDto) @Type(() => OrderItemCreateDto)
    @IsOptional()
    readonly order_info?: OrderItemDto[] | OrderItemCreateDto[];
}

export class OrderDto extends OrderCreateDto {
    @IsInt()
    readonly id: number;

    @IsInt()
    readonly total_amount: number;

    @ValidateNested({ each: true }) @Type(() => OrderItemDto)
    order_info: OrderItemDto[];
}
