import { BadRequestException, Inject, Injectable, NestMiddleware } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { NextFunction, Request, Response } from "express";
import { AuthDiToken } from "./auth.di";
import { CurrentUserService } from "./services";
import * as nconf from "nconf";

interface TokenDto {
    exp: string;
    iat: string;
    payload: any;
}

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor(
        @Inject(AuthDiToken.CURRENT_USER_SERVICE) private readonly currentUserService: CurrentUserService,
        private readonly jwtService: JwtService
    ) {
    }

    use(req: Request, res: Response, next: NextFunction) {
        const {cookies} = req;
        const {maxAge, sameSite, payloadKey, signatureKey} = nconf.get("cookieOptions");

        if (!cookies) {
            this.currentUserService.set(null);
            throw new BadRequestException("Could not verify token.");
        }

        const payloadCookie = cookies[payloadKey];
        const signatureCookie = cookies[signatureKey];

        if (payloadCookie && signatureCookie) {
            try {
                const jwtToken = `${payloadCookie}.${signatureCookie}`;
                if (this.jwtService.verify(jwtToken)) {
                    const {payload: userData} = this.jwtService
                        .decode(jwtToken, {complete: true}) as TokenDto;

                    // extend the life of the cookie
                    const newToken = this.jwtService.sign(userData);

                    const [header, payload] = newToken.split(".");
                    res.cookie(payloadKey, `${header}.${payload}`, {
                        maxAge,
                        sameSite,
                    });

                    this.currentUserService.set({...userData});
                }
            } catch (e) {
                this.currentUserService.set(null);
                throw new BadRequestException("Could not verify token.");
            }
        } else {
            this.currentUserService.set(null);
        }
        next();
    }
}
