import { Injectable } from "@nestjs/common";
import { BaseService } from "../../../../base.service";

@Injectable()
export class PermissionService extends BaseService {

    static getPermissionsRelatedMethods(role): string[] {
        const arr = [];
        if (role.can_add) arr.push("POST");
        if (role.can_view) arr.push("GET");
        if (role.can_delete) arr.push("DELETE");
        if (role.can_edit) arr.push("PUT");
        return arr;
    }

    static hasSuperAccess(role): boolean {
        return role.can_add && role.can_view && role.can_edit && role.can_delete;
    }
}
