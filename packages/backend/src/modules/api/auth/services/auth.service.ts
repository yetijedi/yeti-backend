import { Inject, Injectable, ForbiddenException, LoggerService, BadRequestException, NotFoundException } from "@nestjs/common";

import * as _ from "lodash";

import { AuthDiToken } from "../auth.di";
import { LoggerDiToken } from "../../../logger";
import { PasswordService } from "./password.service";
import { ConfigDiToken, Config } from "../../../config";
import { UserLoginDto, JwtUserData, ResetPasswordDto } from "../auth.interfaces";
import { UserDiToken, UserService, UserDto } from "../../user";
import { ValidatorService, NotEmptyValidator, ValidationError } from "../../../shared/validator";
import { UserUpdatePasswordDto, getFullNameSafe } from "../../user/dto";
import { CurrentUserService } from "./current-user.service";
import { DbDiToken, ResourceRole, Resource } from "../../../db";
import { IFindOptions } from "sequelize-typescript";
import { PermissionService } from "./permission.service";
import { JwtService } from "@nestjs/jwt";
import { EntityNotFoundException } from "../../../../http/exceptions";

@Injectable()
export class AuthService {
    public static SECRET: string;

    public constructor(
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config,
        @Inject(UserDiToken.USER_SERVICE) private readonly userService: UserService,
        @Inject(LoggerDiToken.LOGGER) private readonly logger: LoggerService,
        @Inject(AuthDiToken.PASSWORD_SERVICE) private readonly passwordService: PasswordService,
        @Inject(AuthDiToken.CURRENT_USER_SERVICE) private readonly currentUserService: CurrentUserService,
        @Inject(DbDiToken.RESOURCE_ROLE_REPOSITORY) private readonly resourceRoleRepository: typeof ResourceRole,
        private readonly jwtService: JwtService
    ) {
        AuthService.SECRET = this.config.auth.tokenSecret;
        if (!AuthService.SECRET) {
            throw new Error(`No token secret specified in the configuration`);
        }
    }

/*
    public async updatePassword(id: number, data: UserUpdatePasswordDto): Promise<void> {
        const errors: ValidationError[] = ValidatorService.compose([
            new NotEmptyValidator("New Password", "newPassword"),
            new NotEmptyValidator("Old Password", "newPassword"),
        ], [], { failOnError: false })(data) as ValidationError[];

        if (!_.isEmpty(errors)) {
            throw new ForbiddenException(errors.join("\n"));
        }

        const user: JwtUserData = this.currentUserService.get();
        // check if user edits himself
        if (!user || user.id !== id) {
            throw new ForbiddenException();
        }

        const userDto: UserDto = await this.userService.getById(id, true);
        this.passwordService.verify(data.oldPassword, userDto.password);
        const newPasswordHash: string = this.passwordService.hash(data.newPassword);
        await this.userService.updatePasswordHash({ id, password: newPasswordHash });
    }
*/
/*
    public async verifyResetPassword(token: string): Promise<void> {
        let payload: { userId: number };
        try {
            payload = this.jwtService.verify(token) as any;
        } catch (err) {
            throw new ForbiddenException();
        }

        const existingUser: UserDto = await this.userService.getById(payload.userId); // checks if user exists
        const userPassword: string = await this.passwordService.generate();
        const password: string = this.passwordService.hash(userPassword);
        const userToUpdate: UserDto = { ...existingUser, password };

        return await this.userService.updatePasswordHash(userToUpdate as any);
    }
*/

    public async login(userInfo: UserLoginDto): Promise<string> {
        const user: UserDto = await this.userService.getByPhoneEmail(userInfo.phone);
        if (!user) {
            throw new EntityNotFoundException("User", undefined, "User with specified credentials not found");
        }
        this.passwordService.verify(userInfo.password, user.password);

        const permissions = await this.getPermissions(user);
        const token: string = this.createAuthToken(user, permissions, userInfo.rememberMe);

        this.userService.updateLoginDate(user.id); // no need to wait for update
        return token;
    }

    public async sendResetPasswordEmail(data: ResetPasswordDto): Promise<void> {
        const user: UserDto = await this.userService.getByPhoneEmail(data.phone);
        const resetPasswordToken = this.createResetPasswordToken(user);
        console.log(resetPasswordToken);
        // return this.emailNotificationService.notify(emailPayload);
    }

    private async getPermissions(user: UserDto): Promise<any> {
        const permissions: any = {};

        // get roles and resources
        const roles = await this.resourceRoleRepository.findAll({
            include: [{
                model: Resource,
                attributes: ["code"]
            }],
            where: {
                role_id: user.role_id
            }
        } as IFindOptions<ResourceRole>);

        roles.forEach(r => {
            const role = r.toJSON();
            const { code: resCode } = role.resource;

            const superAccess = PermissionService.hasSuperAccess(role);
            if (superAccess) {
                permissions[resCode] = true;
            } else {
                permissions[resCode] = [
                    ...PermissionService.getPermissionsRelatedMethods(role)
                ];
            }
        });
        return permissions;
    }

    private createAuthToken(user: UserDto, permissions: any, rememberMe?: boolean): string {
        // const tokenExpiration = rememberMe ? this.config.auth.longTokenExpiration : this.config.auth.tokenExpiration;
        const userFullName: string = getFullNameSafe(user.first_name, user.last_name);
        const payload: JwtUserData = {
            id: user.id,
            name: userFullName,
            email: user.email,
            phone: user.phone,
            shop_id: user.shop_id,
            role: {
                id: user.role.id,
                dashboardType: user.role.dashboard_type,
                name: user.role.name
            },
            permissions
        };
        return this.generateToken(payload);
    }

    private generateToken(payload: any): string {
        return this.jwtService.sign(payload);
    }

    // public async register(user: UserRegistrationDto): Promise<UserDto> {
    //     const errors: ValidationError[] = ValidatorService.compose([
    //         new NotEmptyValidator("Phone", "phone"),
    //         new NotEmptyValidator("Role", "role"),
    //         // new NotEmptyValidator("Email", "contact.email"),
    //         new NotEmptyValidator("Password", "password"),
    //     ], [], { failOnError: false })(user) as ValidationError[];
    //
    //     if (!_.isEmpty(errors)) {
    //         throw new UserRegistrationException(_.map(errors , e => e.message).join("\n"));
    //     }
    //
    //     // TODO: validate request: password is strong
    //
    //     const passwordHash: string = await this.passwordService.hash(user.password);
    //     // const contact: ContactDto = await this.contactService.create(user.contact);
    //     const userToCreate = _.assign(
    //         {},
    //         _.pick(user, ["phone", "role"]),
    //         { password: passwordHash },
    //     );
    //     const createdUser: UserDto = _.assign({}, await this.userService.create(userToCreate as any));
    //
    //     // await this.sendInviteEmail(createdUser);
    //
    //     return _.omit(createdUser, "password") as UserDto;
    // }

    private createResetPasswordToken(user: UserDto): string {
        const inviteTokenPayload = { userId: user.id };
        // const { resetPasswordTokenExpiration, resetPasswordTokenSecret } = this.config.auth;
        // @TODO: add { expiresIn: resetPasswordTokenExpiration, secret: resetPasswordTokenSecret } as a config
        return this.generateToken(inviteTokenPayload);
    }

}
