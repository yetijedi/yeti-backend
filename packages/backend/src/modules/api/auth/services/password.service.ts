import { Injectable } from "@nestjs/common";
import { hashSync, compareSync, genSaltSync } from "bcrypt-nodejs";
import { generate } from "generate-password";

import { InvalidPasswordException } from "../exceptions";


@Injectable()
export class PasswordService {

    public async generate(): Promise<string> {
        return generate({ strict: true });
    }

    public hash(password: string): string {
        return hashSync(password, genSaltSync());
    }

    public verify(password: string, hashString: string): void {
        if (!compareSync(password, hashString)) {
            throw new InvalidPasswordException();
        }
    }
}
