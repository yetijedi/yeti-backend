export interface UserLoginDto {
    phone: string;
    password: string;
    rememberMe?: boolean;
}

export class ResetPasswordDto {
    phone: string;
}

export interface JwtUserData {
    id: number;
    name: string;
    email: string;
    phone: string;
    role: any;
    shop_id: number;
    permissions: string[];
}
