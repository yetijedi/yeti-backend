import { Body, Controller, HttpCode, HttpStatus, Inject, Post, Req, Res, } from "@nestjs/common";

import { Public } from "../../../http/decorators";
import { AuthService, CurrentUserService } from "./services";
import { AuthDiToken } from "./auth.di";
import { ResetPasswordDto, UserLoginDto } from "./auth.interfaces";
import { Request, Response } from "express";
import { Config, ConfigDiToken } from "../../config";

@Controller("/auth")
// @UseFilters(InvalidPasswordExceptionFilter)
export class AuthController {

    public constructor(
        @Inject(ConfigDiToken.CONFIG) private readonly config: Config,
        @Inject(AuthDiToken.AUTH_SERVICE) private readonly authService: AuthService,
        @Inject(AuthDiToken.CURRENT_USER_SERVICE) private readonly currentUserService: CurrentUserService,
    ) {
    }

    @Post("/login")
    @Public()
    public async login(
        @Body() userInfo: UserLoginDto,
        @Req() req: Request,
        @Res() res: Response
    ): Promise<any> {
        const token = await this.authService.login(userInfo);
        const {maxAge, sameSite, payloadKey, signatureKey} = this.config.cookieOptions;

        const [header, payload, signature] = token.split(".");

        res.cookie(payloadKey, `${header}.${payload}`, {
            maxAge, // 30min
            sameSite,
        });

        res.cookie(signatureKey, signature, {
            httpOnly: true,
            sameSite,
        });

        return res.sendStatus(HttpStatus.CREATED);
    }

    @HttpCode(HttpStatus.ACCEPTED)
    @Post("/logout")
    @Public()
    public async logout(@Res() res: Response): Promise<void> {
        const {payloadKey, signatureKey} = this.config.cookieOptions;
        res.clearCookie(payloadKey);
        res.clearCookie(signatureKey);
        res.sendStatus(HttpStatus.ACCEPTED);
    }

    // @HttpCode(200)
    // @Post("/register")
    // @Public()
    // public async register(@Body() data: UserRegistrationDto): Promise<UserDto> {
    //     return this.authService.register(data);
    // }
    //
    @HttpCode(200)
    @Post("/password/reset")
    @Public()
    public async resetPassword(@Body() data: ResetPasswordDto): Promise<void> {
        return this.authService.sendResetPasswordEmail(data);
    }

    // @HttpCode(200)
    // @Post("/password/reset/verify")
    // @Public()
    // public async verifyResetPassword(@Body("token") token: string): Promise<void> {
    //     return this.authService.verifyResetPassword(token);
    // }

    // @Put("/users/:id/password")
    // public updatePassword(
    //     @Body() data: UserUpdatePasswordDto,
    //     @Param("id", new ParseIntPipe()) id: number,
    // ): Promise<void> {
    //     return this.authService.updatePassword(id, data);
    // }
}
