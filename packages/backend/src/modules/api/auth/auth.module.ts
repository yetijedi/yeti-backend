import { Module, NestModule, MiddlewareConsumer, RequestMethod, forwardRef, Inject, Global } from "@nestjs/common";
import { DbModule } from "../../db";
import { UserModule } from "../user";
import { Config, ConfigDiToken, ConfigModule } from "../../config";
import { authProviders } from "./auth.providers";
import { AuthController } from "./auth.controller";
import { AuthMiddleware } from "./auth.middleware";
import { JwtModule } from "@nestjs/jwt";

@Module({
    imports: [
        ConfigModule,
        DbModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: (config: Config) => ({
                secret: config.auth.tokenSecret,
            }),
            inject: [ConfigDiToken.CONFIG],
        }),
        forwardRef(() => UserModule)
    ],
    providers: [...authProviders],
    exports: [...authProviders],
    controllers: [AuthController],
})
export class AuthModule implements NestModule {
    public configure(consumer: MiddlewareConsumer): void | MiddlewareConsumer {
        consumer
            .apply(AuthMiddleware)
            .forRoutes({ path: "*", method: RequestMethod.ALL });
    }
}
