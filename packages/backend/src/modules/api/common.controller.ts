import { Controller, Get, Inject, UseGuards, } from "@nestjs/common";
import { AuthorizeGuard } from "../../http/guards";
import { ALL_WEEK_DAYS, WORKING_WEEK_DAYS } from "../shared/constants/week-days";
import { RESOURCE_CODES } from "../shared/constants/resources";
import { WorkspaceService } from "./workspace";
import { DbDiToken } from "../db";
import { VIRTUAL_SHOP_ID } from "../db/seeder/shop";
import { DashboardType } from "../shared/dashboard.enum";
import { DEFAULT_CATEGORY_ID } from "../db/seeder/category";

interface ISettings {
    workingWeekDays: number;
    allWeekDays: number;
    resourceCodes: typeof RESOURCE_CODES;
    defaultShopId: number;
    defaultCategoryId: number;
    dashboards: any
}

@Controller("/common")
@UseGuards(AuthorizeGuard)
export class CommonController {
    public constructor(
        @Inject(DbDiToken.DICTIONARY_REPOSITORY) private readonly dictionaryRepository: WorkspaceService,
    ) {
    }


    @Get("/settings")
    public getSettings(): ISettings {
        return {
            workingWeekDays: WORKING_WEEK_DAYS,
            allWeekDays: ALL_WEEK_DAYS,
            resourceCodes: RESOURCE_CODES,
            defaultShopId: VIRTUAL_SHOP_ID,
            defaultCategoryId: DEFAULT_CATEGORY_ID,
            dashboards: [{
                    id: DashboardType.ADMINPANEL,
                    value: 'ADMINPANEL'
                },{
                    id: DashboardType.WORKSPACE,
                    value: 'WORKSPACE'
                }
            ]


        }
    }

    @Get("/units")
    public getDictionaryTypes() {
    //    return units
    }
}
