import { CUSTOM_SHOP_ID } from "./shop";
import { EMPLOYER } from "./user";

export const userShop = [
    {
        shop_id: CUSTOM_SHOP_ID,
        user_id: EMPLOYER.id,
        active: 1
    }
];
