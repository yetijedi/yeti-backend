import { RESOURCE_CODES } from "../../shared/constants/resources";

let id = 1;
export const resources = Object.keys(RESOURCE_CODES).map(
    key => ({
        id: id++,
        name: key,
        code: RESOURCE_CODES[key]
    })
);
