import * as moment from "moment";
import { ALL_WEEK_DAYS, WEEK_DAYS_BITMAP as w } from "../../shared/constants/week-days";

export const VIRTUAL_SHOP_ID = 1;
export const CUSTOM_SHOP_ID = 2;

export const shops = [
    {
        name: "VIRTUAL_SHOP",
        working_days: ALL_WEEK_DAYS,
    },
    {
        name: "Custom Shop",
        working_days: w.MON + w.THU + w.FRI,
        open_at: moment().set("hour", 10).format("HH:mm"),
        close_at:  moment().set("hour", 22).format("HH:mm"),
    }
];
