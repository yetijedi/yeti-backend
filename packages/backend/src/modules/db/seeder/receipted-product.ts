export const receipted_products = [
    {
        receipt_product_id: 1,
        ingredient_product_id: 3,
        weight: 1
    }, {
        receipt_product_id: 1,
        ingredient_product_id: 4,
        weight: 0.5
    }, {
        receipt_product_id: 2,
        ingredient_product_id: 4,
        weight: 0.2
    }
];
