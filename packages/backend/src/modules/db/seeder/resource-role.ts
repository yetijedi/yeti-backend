import { RESOURCE_CODES } from "../../shared/constants/resources";
import { ADMIN_ROLE, TERMINAL_ROLE } from "./role";

const resources = Object.keys(RESOURCE_CODES);
const adminRoles = resources
    .map((res, i) => ({
        resource_id: i + 1,
        role_id: ADMIN_ROLE.id,
        can_add: 1,
        can_delete: 1,
        can_edit: 1,
        can_view: 1,
    }));

export const resourceRoles = [
    ...adminRoles,
    {
        role_id: TERMINAL_ROLE.id,
        resource_id: 1,
        can_view: 1,
        can_add: 1,
    },{
        role_id: TERMINAL_ROLE.id,
        resource_id: 2,
        can_view: 1,
    },{
        role_id: TERMINAL_ROLE.id,
        resource_id: 4,
        can_view: 1,
    },{
        role_id: TERMINAL_ROLE.id,
        resource_id: 5,
        can_view: 1,
    },{
        role_id: TERMINAL_ROLE.id,
        resource_id: 6,
        can_view: 1,
    }

];
