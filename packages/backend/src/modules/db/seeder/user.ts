import { ADMIN_ROLE, TERMINAL_ROLE } from "./role";
import { CUSTOM_SHOP_ID } from "./shop";
import { SalaryTypes } from "../../api/user/dto";

export const ADMIN = {
    id: 1,
    first_name: "Robert",
    last_name: "Keyan",
    email: "robert@keyan.blog",
    phone: "+37477545347",
    role_id: ADMIN_ROLE.id,
    salary: 0,
    salary_type: SalaryTypes.FIXED,
    password: `$2a$10$BcTYJZz4zgl/cJwe24F4AeKhlG5eZPIOjLfnTT4kdHEc1n3Aemnhq`, // admin
};

export const EMPLOYER = {
    id: 2,
    first_name: "Shop",
    last_name: "Account",
    email: "terminal@test.com",
    phone: "+7777777",
    role_id: TERMINAL_ROLE.id,
    salary: 0,
    shop_id: CUSTOM_SHOP_ID,
    salary_type: SalaryTypes.FIXED,
    password: `$2a$10$BcTYJZz4zgl/cJwe24F4AeKhlG5eZPIOjLfnTT4kdHEc1n3Aemnhq`, // admin
};

export const users = [
    ADMIN,
    EMPLOYER
];
