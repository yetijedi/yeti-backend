export const VIRTUAL_DICTIONARY = {
    id: 1,
    code_name: "V_DEFAULT_DICT",
    formatted_name: "V_DEFAULT_DICT"
};

export const UNIT_DICT = { id: 2, code_name: "UNIT", formatted_name: "unit" };
export const G_DICT = { id: 3, code_name: "G", formatted_name: "g" };
export const KG_DICT = { id: 4, code_name: "KG", formatted_name: "kg" };
export const ML_DICT = { id: 5, code_name: "ML", formatted_name: "ml" };
export const L_DICT = { id: 6, code_name: "L", formatted_name: "l" };

export const dictionaries = [
    VIRTUAL_DICTIONARY,
    UNIT_DICT,
    G_DICT,
    KG_DICT,
    ML_DICT,
    L_DICT
];
