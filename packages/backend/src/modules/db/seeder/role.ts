import { DashboardType } from "../../shared/dashboard.enum";

export const ADMIN_ROLE = {
    id: 1,
    name: "admin",
    dashboard_type: DashboardType.ADMINPANEL
};

export const TERMINAL_ROLE = {
    id: 2,
    name: "Terminal for shop",
    dashboard_type: DashboardType.WORKSPACE
};

export const roles = [
    ADMIN_ROLE,
    TERMINAL_ROLE
];
