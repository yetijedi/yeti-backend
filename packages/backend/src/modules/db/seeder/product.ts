import { CUSTOM_SHOP_ID, VIRTUAL_SHOP_ID } from "./shop";
import { KG_DICT, L_DICT, UNIT_DICT } from "./dictionary";
import { ACTIVE_CATEGORY_ID, DEFAULT_CATEGORY_ID, PASSIVE_CATEGORY_ID } from "./category";

export const products = [
    {
        name: "Common Product",
        description: "lorem ipsum",
        available: 150,
        unit_type_id: UNIT_DICT.id, // unit
        category_id: DEFAULT_CATEGORY_ID, // selling product
        shop_id: VIRTUAL_SHOP_ID, // Default shop
        native_price: 100,
        sell_price: 200
    }, {
        name: "Product 1",
        description: "lorem ipsum",
        available: 150,
        unit_type_id: UNIT_DICT.id, // unit
        category_id: ACTIVE_CATEGORY_ID, // selling product
        shop_id: CUSTOM_SHOP_ID, // Custom shop
        native_price: 300,
        sell_price: 500
    },{
        name: "Product 2",
        description: "lorem ipsum",
        available: 150,
        unit_type_id: UNIT_DICT.id, // unit
        category_id: ACTIVE_CATEGORY_ID, // selling product
        shop_id: CUSTOM_SHOP_ID, // Custom shop
        native_price: 300,
        sell_price: 500
    },{
        name: "Product 3",
        description: "lorem ipsum",
        available: 150,
        unit_type_id: UNIT_DICT.id, // unit
        category_id: ACTIVE_CATEGORY_ID, // selling product
        shop_id: CUSTOM_SHOP_ID, // Custom shop
        native_price: 300,
        sell_price: 500
    },{
        name: "Product 4",
        description: "lorem ipsum",
        available: 150,
        unit_type_id: UNIT_DICT.id, // unit
        category_id: ACTIVE_CATEGORY_ID, // selling product
        shop_id: CUSTOM_SHOP_ID, // Custom shop
        native_price: 300,
        sell_price: 500
    },{
        name: "Product 5",
        description: "lorem ipsum",
        available: 150,
        unit_type_id: UNIT_DICT.id, // unit
        category_id: ACTIVE_CATEGORY_ID, // selling product
        shop_id: CUSTOM_SHOP_ID, // Custom shop
        native_price: 300,
        sell_price: 500
    }, {
        name: "Ingredient 1",
        description: "lorem ipsum",
        available: 10,
        unit_type_id: L_DICT.id, // L
        category_id: PASSIVE_CATEGORY_ID, // ingredient
        shop_id: VIRTUAL_SHOP_ID,
        native_price: 100
    }, {
        name: "Ingredient 2",
        description: "lorem ipsum",
        available: 100,
        unit_type_id: KG_DICT.id, // KG
        category_id: PASSIVE_CATEGORY_ID,
        shop_id: CUSTOM_SHOP_ID,
        native_price: 20
    }
];
