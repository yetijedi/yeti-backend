import {SalesPlanDateDto} from "../../api/sales-plan/dto";
import * as moment from "moment";
import { SalesPlanStatuses } from "../../api/sales-plan/dto";

export const salesPlanDates = [
    new SalesPlanDateDto({
        shop_sales_plan_id: 1,
        date: moment([2018, 9, 10]).toDate(),
        success_point: 500,
        status: SalesPlanStatuses.NOT_STARTED,
        progress: 0
    }),
    new SalesPlanDateDto({
        shop_sales_plan_id: 1,
        date: moment([2018, 9, 11]).toDate(),
        success_point: 500,
        status: SalesPlanStatuses.NOT_STARTED,
        progress: 0
    }),
    new SalesPlanDateDto({
        shop_sales_plan_id: 1,
        date: moment([2018, 9, 12]).toDate(),
        success_point: 500,
        status: SalesPlanStatuses.NOT_STARTED,
        progress: 0
    }),
    new SalesPlanDateDto({
        shop_sales_plan_id: 1,
        date: moment([2018, 9, 13]).toDate(),
        success_point: 500,
        status: SalesPlanStatuses.NOT_STARTED,
        progress: 0
    }),
];
