let id = 0;

class OrderItemSeeder {
    public id: number;
    public order_id: number;
    public product_id: number;
    public quantity: number;
    public native_price: number;
    public sell_price: number;

    constructor(orderId, productId, quantity, nativePrice, sellPrice) {
        this.id = ++id; // it's incremental for each order-item
        this.order_id = orderId;
        this.product_id = productId;
        this.quantity = quantity;
        this.native_price = nativePrice;
        this.sell_price = sellPrice;
    }
}

export const shop1OrderItems = [
    new OrderItemSeeder(1, 1, 2, 100, 200),
    new OrderItemSeeder(1, 1, 2, 100, 200)
];

export const shop2OrderItems = [
    new OrderItemSeeder(1, 1, 2, 100, 200),
    new OrderItemSeeder(1, 2, 2, 300, 500)
];

export const shop2OrderItems2 = [
    new OrderItemSeeder(2, 2, 5, 300, 500),
];

export const orderItems = [
    ...shop1OrderItems,
    ...shop2OrderItems,
    ...shop2OrderItems2
];
