import * as moment from "moment";

export const salesPlans = [
    {
        name: "CUSTOM SALES PLAN",
        description: "Custom sales plan",
        total_success_point: 2000,
        start_date: moment([2018, 9, 10]).toDate(),
        end_date: moment([2018, 9, 13]).toDate(),
        dates: [
            {date: moment([2018, 9, 10]).toDate(), success_point: 500},
            {date: moment([2018, 9, 11]).toDate(), success_point: 500},
            {date: moment([2018, 9, 12]).toDate(), success_point: 500},
            {date: moment([2018, 9, 13]).toDate(), success_point: 500},
        ]
    }
];
