import { users } from "./user";
import { roles } from "./role";
import { shops } from "./shop";
import { products } from "./product";
import { categories } from "./category";
import {
    User,
    Product,
    Category,
    Shop,
    Role,
    SalesPlanDate,
    ShopSalesPlan,
    Dictionary,
    UserShop,
    ReceiptedProduct,
    ResourceRole,
    Resource
} from "../entities";
import {shopSalesPlans} from "./shop-sales-plan";
import {salesPlanDates} from "./sales-plan-date";
import { dictionaries } from "./dictionary";
import { userShop } from "./user-shop";
import { receipted_products } from "./receipted-product";
import { resources } from "./resource";
import { resourceRoles } from "./resource-role";

export async function init() {
    try {
        await Resource.bulkCreate(resources);
        await Role.bulkCreate(roles);
        await ResourceRole.bulkCreate(resourceRoles);
        await Dictionary.bulkCreate(dictionaries);
        await Shop.bulkCreate(shops);
        await User.bulkCreate(users);
        await Category.bulkCreate(categories);
        await Product.bulkCreate(products);
        await ReceiptedProduct.bulkCreate(receipted_products);
        // await Order.bulkCreate(orders);
        // await OrderItem.bulkCreate(orderItems);
        // await SalesPlan.bulkCreate(salesPlans);
        await ShopSalesPlan.bulkCreate(shopSalesPlans);
        await SalesPlanDate.bulkCreate(salesPlanDates);
        await UserShop.bulkCreate(userShop);
    } catch (e) {
        console.log(e);
    }

}
