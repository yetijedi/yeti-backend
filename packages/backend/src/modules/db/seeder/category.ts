export const DEFAULT_CATEGORY_ID = 1;
export const PASSIVE_CATEGORY_ID = 2;
export const ACTIVE_CATEGORY_ID = 3;

export const categories = [
    {
        name: "DEFAULT CATEGORY",
    },
    {
        name: "Ingredients",
        description: "Default ingredients",
    },
    {
        name: "Sell Products",
        description: "Sell products",
    }
];
