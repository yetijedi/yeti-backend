import { shop1OrderItems, shop2OrderItems, shop2OrderItems2 } from "./order-item";
import { CUSTOM_SHOP_ID } from "./shop";

export const orders = [
    {
        shop_id: CUSTOM_SHOP_ID,
        total_amount: 800,
        order_info: [
            ...shop1OrderItems
        ]
    }, {
        shop_id: CUSTOM_SHOP_ID,
        total_amount: 1400,
        order_info: [
            ...shop2OrderItems
        ]
    }, {
        shop_id: CUSTOM_SHOP_ID,
        total_amount: 2500,
        order_info: [
            ...shop2OrderItems2
        ]
    }
];
