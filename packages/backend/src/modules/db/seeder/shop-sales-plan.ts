import { CUSTOM_SHOP_ID } from "./shop";
import { SalesPlanStatuses } from "../../api/sales-plan/dto";
import * as moment from "moment";

export const shopSalesPlans = [
    {
        name: "CUSTOM SALES PLAN",
        description: "Custom sales plan",
        total_success_point: 2000,
        start_date: moment([2018, 9, 10]).toDate(),
        end_date: moment([2018, 9, 13]).toDate(),
        shop_id: CUSTOM_SHOP_ID, // custom shop
        shop_progress: 0,
        status: SalesPlanStatuses.NOT_STARTED
    }
];
