import { Sequelize } from "sequelize-typescript";

import { DbDiToken } from "./db.di";
import { Config, ConfigDiToken } from "../config";
import {
    User,
    Contact,
    File,
    TimeBasedEvent,
    Product, Category, Shop, Order, Role, ShopSalesPlan, Expense, SalesPlanDate, Dictionary, UserShop, ReceiptedProduct, Resource, ResourceRole
} from "./entities";
import { init } from "./seeder";
import {OrderItem} from "./entities/order-item.entity";

const repositoryProviders = [
    {
        provide: DbDiToken.ROLES_REPOSITORY,
        useValue: Role,
    },
    {
        provide: DbDiToken.DICTIONARY_REPOSITORY,
        useValue: Dictionary,
    },
    {
        provide: DbDiToken.USER_REPOSITORY,
        useValue: User,
    },
    {
        provide: DbDiToken.CONTACT_REPOSITORY,
        useValue: Contact,
    },
    {
        provide: DbDiToken.FILE_REPOSITORY,
        useValue: File,
    },
    {
        provide: DbDiToken.SHOP_REPOSITORY,
        useValue: Shop,
    },
    {
        provide: DbDiToken.CATEGORY_REPOSITORY,
        useValue: Category,
    },
    {
        provide: DbDiToken.PRODUCT_REPOSITORY,
        useValue: Product,
    },
    {
        provide: DbDiToken.RECEIPTED_PRODUCT_REPOSITORY,
        useValue: ReceiptedProduct,
    },
    {
        provide: DbDiToken.ORDER_REPOSITORY,
        useValue: Order,
    },
    {
        provide: DbDiToken.ORDER_ITEM_REPOSITORY,
        useValue: OrderItem,
    },
    {
        provide: DbDiToken.USER_SHOP_REPOSITORY,
        useValue: UserShop,
    },
    {
        provide: DbDiToken.SHOP_SALES_PLAN_REPOSITORY,
        useValue: ShopSalesPlan,
    },
    {
        provide: DbDiToken.SALES_PLAN_DATE,
        useValue: SalesPlanDate,
    },
    {
        provide: DbDiToken.RESOURCE_REPOSITORY,
        useValue: Resource,
    },
    {
        provide: DbDiToken.RESOURCE_ROLE_REPOSITORY,
        useValue: ResourceRole,
    },
    {
        provide: DbDiToken.EXPENSE_REPOSITORY,
        useValue: Expense,
    },
    {
        provide: DbDiToken.TIME_BASED_EVENT_REPOSITORY,
        useValue: TimeBasedEvent,
    }
];

export const dbProviders = [
    ...repositoryProviders,
    {
        provide: DbDiToken.SEQUELIZE_CONNECTION,
        useFactory: async (config: Config) => {
            const sequelize = new Sequelize(config.db);
            sequelize.addModels(
                [
                    Contact,
                    Role,
                    User,
                    File,
                    Shop,
                    Category,
                    Product,
                    ReceiptedProduct,
                    Order,
                    OrderItem,
                    ShopSalesPlan,
                    SalesPlanDate,
                    Resource,
                    ResourceRole,
                    Expense,
                    Dictionary,
                    TimeBasedEvent,
                    UserShop
                ],
            );
            // @ts-ignore
            await sequelize.sync({force: true});
            await init();
            return sequelize;
        },
        inject: [ConfigDiToken.CONFIG],
    },
];
