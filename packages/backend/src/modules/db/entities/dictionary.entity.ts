import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    AllowNull,
    DeletedAt,
} from "sequelize-typescript";
// @ts-ignore
@Table({
    tableName: "dictionaries",
})
export class Dictionary extends Model<Dictionary> {
    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof Dictionary)[] = [
        "code_name",
        "formatted_name"
    ];

    @AllowNull(false)
    @Column(DataType.STRING)
    code_name: string;

    @AllowNull(false)
    @Column(DataType.STRING)
    formatted_name: string;

    @DeletedAt
    is_deleted?: Date;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
