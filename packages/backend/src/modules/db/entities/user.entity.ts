import * as _ from "lodash";
import { AllowNull, BelongsTo, Column, CreatedAt, DataType, Default, DeletedAt, ForeignKey, Model, Table, UpdatedAt, } from "sequelize-typescript";
import { Shop } from "./shop.entity";
import { Role } from "./roles.entity";
import { VIRTUAL_SHOP_ID } from "../seeder/shop";
import { SalaryTypes } from "../../api/user/dto";

const salaryTypes = _.values(SalaryTypes);

// @ts-ignore
@Table({
    tableName: "users"
})
export class User extends Model<User> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof User)[] = [
        "first_name",
        "last_name",
        "phone",
        "email",
        "role_id",
        "shop_id",
        "salary",
        "salary_type",
        "is_active",
    ];

    @AllowNull(false)
    @Column(DataType.STRING)
    first_name: string;

    @AllowNull(false)
    @Column(DataType.STRING)
    last_name: string;

    @AllowNull(false)
    @Column(DataType.STRING)
    email: string;

    @AllowNull(false)
    @Column(DataType.STRING)
    phone: string;

    @AllowNull(false)
    @Column(DataType.STRING)
    password: string;

    @AllowNull(false)
    @ForeignKey(() => Role)
    @Column(DataType.INTEGER)
    role_id: number;

    @BelongsTo(() => Role)
    role: Role;

    @Default(VIRTUAL_SHOP_ID) // virtual shop ID
    @ForeignKey(() => Shop)
    @Column(DataType.INTEGER)
    shop_id: number;
    @BelongsTo(() => Shop)
    shop: Shop;

    @Column({
        type: DataType.INTEGER,
        validate: {
            min: SalaryTypes.PERCENT,
            max: SalaryTypes.FIXED
        }
    })
    salary?: number;

    @Column({
        type: DataType.SMALLINT,
        validate: {
            isIn: [salaryTypes],
        },
    })
    salary_type: SalaryTypes;

    @Default(true)
    @Column(DataType.BOOLEAN)
    is_active?: boolean;

    @Default(Date.now())
    @Column(DataType.DATE)
    last_login?: string;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;

    @DeletedAt
    is_deleted?: Date;
}
