import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt, Default, AllowNull, ForeignKey, BelongsTo, HasMany, DeletedAt
} from "sequelize-typescript";
import { Shop } from "./shop.entity";
import { SalesPlanDate } from "./sales-plan-date.entity";
import { VIRTUAL_SHOP_ID } from "../seeder/shop";
import { SalesPlanStatuses } from "../../api/sales-plan/dto";
import * as UUID from "uuid";
// @ts-ignore
@Table({
    tableName: "shop-sales-plan",
})
export class ShopSalesPlan extends Model<ShopSalesPlan> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof ShopSalesPlan)[] = [
        "name",
        "description",
        "shop_id",
        "shop_progress",
        "status",
        "total_success_point"
    ];

    @Default("Sales plan #" + UUID.v4())
    @AllowNull(false)
    @Column(DataType.STRING)
    name: string;

    @Column(DataType.TEXT)
    description?: string;

    @AllowNull(false)
    @Default(VIRTUAL_SHOP_ID)
    @ForeignKey(() => Shop)
    @Column(DataType.INTEGER)
    shop_id: number;

    @BelongsTo(() => Shop)
    shops: Shop[];

    @Column({
        type: DataType.SMALLINT,
        validate: {
            min: SalesPlanStatuses.NOT_STARTED,
            max: SalesPlanStatuses.FAIL,
        }
    })
    status: SalesPlanStatuses;

    @Default(0)
    @Column(DataType.SMALLINT)
    shop_progress: number;

    @AllowNull(false)
    @Column(DataType.INTEGER)
    total_success_point: number;

    @AllowNull(false)
    @Column(DataType.DATE)
    start_date: Date;

    @AllowNull(false)
    @Column(DataType.DATE)
    end_date: Date;

    @HasMany(() => SalesPlanDate)
    dates: SalesPlanDate[];

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;

    @DeletedAt
    deleted_at?: Date;
}
