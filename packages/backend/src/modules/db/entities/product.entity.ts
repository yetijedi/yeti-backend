import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    ForeignKey,
    BelongsTo,
    Default,
    AllowNull, DeletedAt, HasMany
} from "sequelize-typescript";
import {Category} from "./category.entity";
import {Shop} from "./shop.entity";
import { Dictionary } from "./dictionary.entity";
import { VIRTUAL_SHOP_ID } from "../seeder/shop";
import { ReceiptedProduct } from "./receipted-product.enitity";
import { ForeignKeyOption } from "../utils";
import { DEFAULT_CATEGORY_ID } from "../seeder/category";
// @ts-ignore
@Table({
    tableName: "products",
})
export class Product extends Model<Product> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof Product)[] = [
        "name",
        "description",
        "available",
        "unit_type_id",
        "shop_id",
        "category_id",
        "native_price",
        "sell_price"
    ];

    @AllowNull(false)
    @Column(DataType.STRING)
    name: string;

    @Column(DataType.TEXT)
    description?: string;

    @AllowNull(false)
    @Default(0)
    @Column(DataType.FLOAT) // weight?
    available: number;

    @ForeignKey(() => Dictionary)
    @Column(DataType.INTEGER)
    unit_type_id: number;

    @BelongsTo(() => Dictionary)
    unitType: Dictionary;

    @AllowNull(false)
    @Column(DataType.FLOAT)
    native_price: number;

    @Column(DataType.FLOAT)
    sell_price?: number;

    @AllowNull(false)
    @Default(DEFAULT_CATEGORY_ID)
    @ForeignKey(() => Category)
    @Column({
        type: DataType.INTEGER,
        onDelete: ForeignKeyOption.SET_DEFAULT
    })
    category_id: number;

    @BelongsTo(() => Category)
    category: Category;

    @Default(VIRTUAL_SHOP_ID)
    @ForeignKey(() => Shop)
    @Column(DataType.INTEGER)
    shop_id: number;

    @BelongsTo(() => Shop)
    shop: Shop;

    @HasMany(() => ReceiptedProduct, { foreignKey: "ingredient_product_id"})
    ingredients: ReceiptedProduct[];

    @HasMany(() => ReceiptedProduct, { foreignKey: "receipt_product_id"})
    receipts: ReceiptedProduct[];

    @DeletedAt
    is_deleted?: Date;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;

    // @AfterUpdate
    // static async afterUpdateHook(instance: Product): Promise<void> {
    //     if (instance.changed("name")) {
    //         // @FIXME
    //         const products: Product[] = await Product.findAll({
    //             where: Sequelize.where(
    //                 Sequelize.literal("ingredients::jsonb = @>'$.ingredient_product_id' ::jsonb"),
    //                 "" + instance.id
    //             )
    //         });
    //
    //         console.log(products);
    //     }
    // }
}
