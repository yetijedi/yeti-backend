import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt, AllowNull, ForeignKey, BelongsTo, Default
} from "sequelize-typescript";
import {Shop} from "./shop.entity";
import { User } from "./user.entity";
import { VIRTUAL_SHOP_ID } from "../seeder/shop";
// @ts-ignore
@Table({
    tableName: "user-shop",
})
export class UserShop extends Model<UserShop> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof UserShop)[] = [
        "shop_id",
        "user_id",
        "active"
    ];

    @AllowNull(false)
    @Default(VIRTUAL_SHOP_ID)
    @ForeignKey(() => Shop)
    @Column(DataType.INTEGER)
    shop_id: number;

    @BelongsTo(() => Shop)
    shops: Shop[];

    @AllowNull(false)
    @ForeignKey(() => User)
    @Column(DataType.INTEGER)
    user_id: number;

    @BelongsTo(() => User)
    users: User;

    @AllowNull(false)
    @Column(DataType.SMALLINT)
    active: number;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
