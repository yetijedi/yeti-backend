import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt, AllowNull,
} from "sequelize-typescript";
import { DashboardType } from "../../shared/dashboard.enum";

// @ts-ignore
@Table({
    tableName: "roles",
})
export class Role extends Model<Role> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof Role)[] = [
        "name",
        "description"
    ];

    @AllowNull(false)
    @Column(DataType.STRING)
    name: string;

    @AllowNull(false)
    @Column({
        type: DataType.INTEGER,
        validate: {
            min: DashboardType.ADMINPANEL,
            max: DashboardType.WORKSPACE
        }
    })
    dashboard_type: string;

    @Column(DataType.TEXT)
    description?: string;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
