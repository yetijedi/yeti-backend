import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    AllowNull,
    DeletedAt,
} from "sequelize-typescript";
import * as _ from "lodash";
import { ExpenseTypes } from "../../api/expense/dto";

export const expenseTypes = _.values(ExpenseTypes);
// @ts-ignore
@Table({
    tableName: "expenses",
})
export class Expense extends Model<Expense> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof Expense)[] = [
        "shop_id",
        "amount",
        "type"
    ];

    @AllowNull(false)
    @Column(DataType.INTEGER)
    shop_id: number;

    @AllowNull(false)
    @Column(DataType.INTEGER)
    amount: number;

    @AllowNull(false)
    @Column({
        type: DataType.ENUM(expenseTypes),
        validate: {
            isIn: [expenseTypes],
        },
    })
    type: ExpenseTypes;

    @AllowNull(false)
    @Column(DataType.INTEGER)
    product_id: number; // for add_product type

    @AllowNull(false)
    @Column(DataType.INTEGER)
    user_id: number; // for salary type

    @DeletedAt
    is_deleted?: Date;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
