import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    AllowNull, Default, DeletedAt,
} from "sequelize-typescript";
// @ts-ignore
@Table({
    tableName: "shops",
})
export class Shop extends Model<Shop> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof Shop)[] = [
        "name",
        "description",
        "working_days",
        "open_at",
        "close_at",
        "current_balance"
    ];

    @AllowNull(false)
    @Column(DataType.STRING)
    name: string;

    @Column(DataType.TEXT)
    description?: string;

    @AllowNull(false)
    @Column(DataType.SMALLINT) // bitmap
    working_days: number;

    @Column(DataType.TIME)
    open_at?: string;

    @Column(DataType.TIME)
    close_at?: string;

    /**
     * @desc all withdraw/deposit actions change this number
     * all sold/bought products change this number
     */
    @Default(0)
    @Column(DataType.INTEGER)
    current_balance: number;

    @DeletedAt
    is_deleted?: Date;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
