import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    ForeignKey,
    BelongsTo,
    DeletedAt, Default,
} from "sequelize-typescript";
import { Resource } from "./resources.entity";
import { Role } from "./roles.entity";
// @ts-ignore
@Table({
    tableName: "resource_role"
})
export class ResourceRole extends Model<ResourceRole> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof ResourceRole)[] = [
        "resource_id",
        "role_id",
        "can_add",
        "can_view",
        "can_edit",
        "can_delete",
    ];

    @ForeignKey(() => Resource)
    @Column(DataType.INTEGER)
    resource_id: number;

    @BelongsTo(() => Resource)
    resource: Resource;

    @ForeignKey(() => Role)
    @Column(DataType.INTEGER)
    role_id: number;

    @BelongsTo(() => Role)
    role: Role;

    @Default(0)
    @Column(DataType.SMALLINT)
    can_add: number;

    @Default(0)
    @Column(DataType.SMALLINT)
    can_view: number;

    @Default(0)
    @Column(DataType.SMALLINT)
    can_edit: number;

    @Default(0)
    @Column(DataType.SMALLINT)
    can_delete: number;

    @DeletedAt
    is_deleted?: Date;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
