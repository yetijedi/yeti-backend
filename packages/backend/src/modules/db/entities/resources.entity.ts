import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt, AllowNull,
} from "sequelize-typescript";
// @ts-ignore
@Table({
    tableName: "resources",
})
export class Resource extends Model<Resource> {

    public id: number;

    @AllowNull(false)
    @Column(DataType.STRING)
    name: string;

    @AllowNull(false)
    @Column(DataType.STRING)
    code: string;

    @Column(DataType.TEXT)
    description?: string;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
