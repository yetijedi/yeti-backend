import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    AllowNull, DeletedAt, Default,
} from "sequelize-typescript";
import * as UUID from "uuid";
// @ts-ignore
@Table({
    tableName: "sales-plan",
})
export class SalesPlan extends Model<SalesPlan> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof SalesPlan)[] = [
        "description",
        "dates",
        "total_success_point"
    ];

    @Default("Sales plan - " + UUID.v4())
    @AllowNull(false)
    @Column(DataType.STRING)
    name: string;

    @AllowNull(false)
    @Column(DataType.STRING)
    description?: string;

    @AllowNull(false)
    @Column(DataType.DATE)
    start_date: Date;

    @AllowNull(false)
    @Column(DataType.DATE)
    end_date: Date;

    @AllowNull(false)
    @Column(DataType.INTEGER)
    total_success_point: number;

    @Column(DataType.JSONB)
    dates: any[];

    @DeletedAt
    deleted_at?: Date;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
