import {
    AllowNull,
    BelongsTo,
    Column,
    DataType,
    Default,
    ForeignKey,
    Model,
    Table,
} from "sequelize-typescript";
import { ShopSalesPlan } from "./shop-sales-plan.entity";
import { SalesPlanStatuses } from "../../api/sales-plan/dto";
// @ts-ignore
@Table({
    tableName: "sales-plan-date",
})
export class SalesPlanDate extends Model<SalesPlanDate> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof SalesPlanDate)[] = [
        "shop_sales_plan_id",
        "date",
        "success_point",
        "status",
        "progress",
    ];

    @AllowNull(false)
    @ForeignKey(() => ShopSalesPlan)
    @Column(DataType.INTEGER)
    shop_sales_plan_id: number;

    @BelongsTo(() => ShopSalesPlan)
    shopSalesPlan: ShopSalesPlan;

    @Column({
        type: DataType.SMALLINT,
        validate: {
            min: SalesPlanStatuses.NOT_STARTED,
            max: SalesPlanStatuses.FAIL
        }
    })
    status: SalesPlanStatuses;

    @AllowNull(false)
    @Default(0)
    @Column(DataType.SMALLINT)
    progress: number;

    @AllowNull(false)
    @Column(DataType.INTEGER)
    success_point: number;

    @AllowNull(false)
    @Column(DataType.DATEONLY)
    date: Date;
}
