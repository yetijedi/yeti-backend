import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    AllowNull,
    ForeignKey, BelongsTo,
} from "sequelize-typescript";
import {Product} from "./product.entity";
import {Order} from "./order.entity";
// @ts-ignore
@Table({
    tableName: "order-item"
})
export class OrderItem extends Model<OrderItem> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof OrderItem)[] = [
        "order_id",
        "product_id",
        "quantity"
    ];

    @AllowNull(false)
    @ForeignKey(() => Order)
    @Column(DataType.INTEGER)
    order_id: number;

    @BelongsTo(() => Order)
    order: Order;

    @AllowNull(false)
    @ForeignKey(() => Product)
    @Column(DataType.INTEGER)
    product_id: number;

    @BelongsTo(() => Product)
    product: Product;

    @AllowNull(false)
    @Column(DataType.FLOAT) // Float for ingredients weights
    quantity: number;

    @AllowNull(false)
    @Column(DataType.FLOAT)
    native_price: number;

    @AllowNull(false)
    @Column(DataType.FLOAT)
    sell_price: number;

    @AllowNull(false)
    @Column(DataType.STRING)
    name: string;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
