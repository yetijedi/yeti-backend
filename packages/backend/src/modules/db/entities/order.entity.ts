import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    AllowNull,
    ForeignKey,
    HasMany,
} from "sequelize-typescript";
import {Shop} from "./shop.entity";
import {OrderItem} from "./order-item.entity";
import { ForeignKeyOption } from "../utils";
// @ts-ignore
@Table({
    tableName: "orders"
})
export class Order extends Model<Order> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof Order)[] = [
        "shop_id",
        "total_amount",
        "order_info"
    ];

    @AllowNull(false)
    @ForeignKey(() => Shop)
    @Column(DataType.INTEGER)
    shop_id: number;

    @AllowNull(false)
    @Column(DataType.INTEGER)
    total_amount: number;

    @Column(DataType.JSONB)
    order_info: OrderItem[];

    @HasMany(() => OrderItem, { onDelete: ForeignKeyOption.CASCADE })
    orderItems: OrderItem[];

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
