import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt, Default, HasMany, AllowNull, ForeignKey, BelongsTo,
} from "sequelize-typescript";
import {Product} from "./product.entity";
import { Dictionary } from "./dictionary.entity";
// @ts-ignore
@Table({
    tableName: "categories"
})
export class Category extends Model<Category> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof Category)[] = [
        "name",
        "description",
        "unit_type_id"
    ];

    @AllowNull(false)
    @Column(DataType.STRING)
    name: string;

    @Column(DataType.TEXT)
    description?: string;

    @ForeignKey(() => Dictionary)
    @Column(DataType.INTEGER)
    unit_type_id: number;

    @BelongsTo(() => Dictionary)
    unitType: Dictionary;

    @HasMany(() => Product)
    products: Product[];

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
