import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    ForeignKey,
    BelongsTo,
    AllowNull,
    DeletedAt,
} from "sequelize-typescript";
import { Product } from "./product.entity";
// @ts-ignore
@Table({
    tableName: "receipted_product"
})
export class ReceiptedProduct extends Model<ReceiptedProduct> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof ReceiptedProduct)[] = [
        "receipt_product_id",
        "ingredient_product_id",
        "weight"
    ];

    @AllowNull(false)
    @ForeignKey(() => Product)
    @Column(DataType.INTEGER)
    receipt_product_id: number;

    @BelongsTo(() => Product, {as: "receiptedProduct", targetKey: "id"})
    receiptedProducts: Product[];

    @AllowNull(false)
    @ForeignKey(() => Product)
    @Column(DataType.INTEGER)
    ingredient_product_id: number;

    @BelongsTo(() => Product, {as: "ingredientProduct", targetKey: "id"})
    ingredientProducts: Product[];

    @AllowNull(false)
    @Column(DataType.FLOAT)
    weight: number;

    @DeletedAt
    is_deleted?: Date;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
