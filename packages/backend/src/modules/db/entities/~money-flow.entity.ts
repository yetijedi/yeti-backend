
import {
    Model,
    Table,
    Column,
    DataType,
    CreatedAt,
    UpdatedAt,
    PrimaryKey, Default,
} from "sequelize-typescript";

/**
 * @NOTE: THIS TABLE REPLACE BY SHOPS => current_balance field
 */
@Table({
    tableName: "money-flow",
})
export class MoneyFlow extends Model<MoneyFlow> {

    public id: number;
    public static PUBLIC_ATTRIBUTES: (keyof MoneyFlow)[] = [
        "current_balance"
    ];

    @PrimaryKey
    @Column(DataType.INTEGER)
    shop_id: number;

    /**
     * @desc all withdraw/deposit actions change this number
     * all sold/bought products change this number
     */
    @Column(DataType.BIGINT)
    @Default(0)
    current_balance: number;

    @CreatedAt
    created_at?: Date;

    @UpdatedAt
    updated_at?: Date;
}
