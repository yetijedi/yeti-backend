import { Module, DynamicModule } from "@nestjs/common";

import { dbProviders } from "./db.providers";
import { ConfigModule, configProviders } from "../config";
import { CipherModule } from "../shared/cipher";


@Module({
    imports: [ConfigModule, CipherModule],
    providers: [...dbProviders, ...configProviders],
    exports: [...dbProviders],
})
export class DbModule {
    static forRoot(): DynamicModule {
        const providers = [...dbProviders];

        return {
            module: DbModule,
            providers,
            exports: providers,
        };
    }
}
