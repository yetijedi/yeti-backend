enum repositoryToken {
    USER_REPOSITORY = "user_repository",
    CONTACT_REPOSITORY = "contact_repository",
    FILE_REPOSITORY = "file_repository",
    TIME_BASED_EVENT_REPOSITORY = "time_based_event",
    PRODUCT_REPOSITORY = "product_repository",
    RECEIPTED_PRODUCT_REPOSITORY = "receipted_product_repository",
    CATEGORY_REPOSITORY = "category_repository",
    SHOP_REPOSITORY = "shop_repository",
    ORDER_REPOSITORY = "order_repository",
    ORDER_ITEM_REPOSITORY = "order_item_repository",
    ROLES_REPOSITORY = "roles_repository",
    SHOP_SALES_PLAN_REPOSITORY = "shop_sales_plan_repository",
    SALES_PLAN_DATE = "sales_plan_date_repository",
    EXPENSE_REPOSITORY = "expense_repository",
    DICTIONARY_REPOSITORY = "dictionary_repository",
    USER_SHOP_REPOSITORY = "user_shop_repository",
    RESOURCE_REPOSITORY = "resource_repository",
    RESOURCE_ROLE_REPOSITORY = "resource_role_repository",
}

export const DbDiToken = Object.freeze({
    SEQUELIZE_CONNECTION: "sequelize_connection",
    ...repositoryToken,
});
