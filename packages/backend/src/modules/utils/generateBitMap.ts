// https://forums.asp.net/t/1635552.aspx?Bitwise+Operations+using+enums
export interface IBitMap {
    [key: string]: number;
}

export function generateBitMap(arr: string[]): IBitMap {
    const result: IBitMap = {};
    arr.map((currentValue, index) => {
        result[currentValue] = Math.pow(2, index);
    });
    return result;
}
