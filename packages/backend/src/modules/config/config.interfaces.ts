export enum Env {
    DEVELOPMENT = "development",
    PRODUCTION = "production",
}

export interface Config {
    env?: Env;
    db?: {
        host: string,
        username: string;
        password: string;
        dialect: any;
        port: number,
        database: string,
        logging?: any,
    };
    storage?: {
        bucket: string,
        prefix: string;
    };
    app?: {
        logRequests?: boolean,
        adminEmail?: string[];
        url?: string;
    };
    auth?: {
        tokenSecret: string;
        inviteTokenSecret: string;
        inviteTokenExpiration: number;
        resetPasswordTokenSecret: string;
        resetPasswordTokenExpiration: number;
    };
    cookieOptions?: {
        maxAge: number;
        sameSite: boolean;
        httpOnly: boolean;
        signatureKey: string;
        payloadKey: string;
    };
    mailgun?: {
        apiKey: string;
        domain: string;
    };
}
