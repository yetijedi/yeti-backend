import { Config } from "../config.interfaces";

const config: Config = {
    db: {
        host: "localhost",
        username: "postgres",
        password: "password",
        port: 5432,
        dialect: "postgres",
        database: "app",
        logging: false
    },
    storage: {
        bucket: "",
        prefix: "",
    },
    app: {
        logRequests: true,
        url: "localhost:4101",
    },
    auth: {
        inviteTokenExpiration: 60 * 60, // 1 hour
        tokenSecret: "yeti_riskey+pp=<3",
        inviteTokenSecret: "f34t6ergdsefgert2we;AWRsadf12rewfsads",
        resetPasswordTokenExpiration: 60 * 60,
        resetPasswordTokenSecret: "fASJfs234rHjkmnXqk324;lm,Xkjade",
    },
    cookieOptions: {
        maxAge: 30 * 60 * 1000, // 30 min
        sameSite: true,
        httpOnly: false,
        signatureKey: "_sign",
        payloadKey: "_payload"
    },
    mailgun: {
        apiKey: "47615f5dedd1fd466e993abad09a0a98-0e6e8cad-8501f824\n",
        domain: "sandbox1acc7df2cc084bbb9793f1fd1786d126.mailgun.org"
    },
};


module.exports = config;
