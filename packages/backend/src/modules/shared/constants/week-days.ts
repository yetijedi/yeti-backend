import {generateBitMap, IBitMap} from "../../utils/generateBitMap";

export const WEEK_DAYS = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"];

export const WEEK_DAYS_BITMAP: IBitMap = generateBitMap(WEEK_DAYS);

export const WORKING_WEEK_DAYS = (() => {
    const { MON, TUE, WED, THU, FRI } = WEEK_DAYS_BITMAP;
    return MON + TUE + WED + THU + FRI;
})();

export const ALL_WEEK_DAYS = (() => {
    const { SAT, SUN } = WEEK_DAYS_BITMAP;
    return WORKING_WEEK_DAYS + SAT + SUN;
})();
