import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from "@nestjs/common";
import { validate } from "class-validator";
import { plainToClass } from "class-transformer";

@Injectable()
export class CommonValidationPipe implements PipeTransform<any> {
    async transform(value, { metatype }: ArgumentMetadata) {
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }
        const object = plainToClass(metatype, value);
        const errors = await validate(object);

        let message = "";
        errors.map((err) => {
            if (err.children) { // for nested arrays
                err.children.forEach(child => {
                    message += JSON.stringify(err.value[ child.property ]) + ", ";
                });
            }

            for (const key in err.constraints) {
                if (err.constraints.hasOwnProperty(key)) {
                    message += err.constraints[key] + ", ";
                }
            }
        });

        if (errors.length > 0) {
            throw new BadRequestException(`Validation failed: ${message}`);
        }
        return value;
    }

    private toValidate(metatype): boolean {
        const types = [String, Boolean, Number, Array, Object];
        return !types.find((type) => metatype === type);
    }
}
