import { BadRequestException, PipeTransform } from "@nestjs/common";

export class ParseIntAsArrayPipe implements PipeTransform<string[]> {
    transform(values: string | string[]) {
        const arr: string[] = !Array.isArray(values) ? [values] : values;
        const parsedValues: number[] = arr.map(val => +val);

        const error: boolean = parsedValues.every(parsedVal => typeof parsedVal !== "number");
        if (error) {
            throw new BadRequestException();
        }
        return parsedValues;
    }

}
