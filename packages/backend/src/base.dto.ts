export class BaseDto {

    public constructor(entity: any) {
        Object.assign(this, { ...entity });
    }

    public created_at?: Date;
    public updated_at?: Date;
    public deleted_at?: Date;
}
